/* SPDX-License-Identifier: LGPL-3.0-or-later OR GPL-2.0 */

/*
 * Copyright (C) 2020 Red Hat, Inc.
 *
 * This file is part of fido2-proxy.
 *
 * fido2-proxy is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * fido2-proxy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see https://www.gnu.org/licenses/.
 */

#include "config.h"

#include <assert.h>
#include <errno.h>
#define GCR_API_SUBJECT_TO_CHANGE 1
#include <gcr/gcr-base.h>
#include <gnutls/abstract.h>
#include <gnutls/crypto.h>
#include <tss2/tss2_tcti.h>
#include <tss2/tss2_tctildr.h>
#include <tss2/tss2_mu.h>
#include <tss2/tss2_esys.h>
#include <tss2/tss2_rc.h>
#include <unistd.h>

#include "log.h"
#include "server.h"

static const char *att_cert_file;
static const char *att_privkey_file;

struct closure {
	bool cancelled;
	struct fp_server *server;
	char *rp;
	uint8_t *client_data_hash;
	size_t client_data_hash_len;

	/* Only used for get_assertion. */
	uint8_t *key_handle;
	size_t key_handle_len;
	bool uv;
};

static void
closure_free(void *p)
{
	struct closure *closure = p;
	if (!closure) {
		return;
	}
	free(closure->rp);
	free(closure->client_data_hash);
	free(closure->key_handle);
	free(closure);
}

static inline void
closure_freep(void *p)
{
	struct closure *closure = *(struct closure **)p;
	closure_free(closure);
}

static inline void *
memdup(const void *src, size_t size)
{
	void *dst = malloc(size);
	if (!dst) {
		return NULL;
	}
	memcpy(dst, src, size);
	return dst;
}

static struct closure *
closure_new(struct fp_server *server, const char *rp,
	    uint8_t *client_data_hash, size_t client_data_hash_len)
{
	_cleanup_(closure_freep) struct closure *p = NULL;
	struct closure *closure;

	p = calloc(1, sizeof(struct closure));
	if (!p) {
		return NULL;
	}
	p->server = server;
	p->rp = strdup(rp);
	if (!p->rp) {
		return NULL;
	}
	p->client_data_hash = memdup(client_data_hash, client_data_hash_len);
	if (!p->client_data_hash) {
		return NULL;
	}
	p->client_data_hash_len = client_data_hash_len;

	closure = p;
	p = NULL;
	return closure;
}

static inline void
esys_freep(void *p)
{
	Esys_Free(*(void **) p);
}

static int
tpm2_start_auth_session(ESYS_CONTEXT *esys, ESYS_TR *handle)
{
	TPM2B_AUTH auth_value = { 0 };
	TPMT_SYM_DEF symmetric = { .algorithm = TPM2_ALG_NULL };
	TSS2_RC rc;

	rc = Esys_StartAuthSession(esys, ESYS_TR_NONE, ESYS_TR_NONE, ESYS_TR_NONE,
				   ESYS_TR_NONE, ESYS_TR_NONE, 0, TPM2_SE_HMAC,
				   &symmetric, TPM2_ALG_SHA256, handle);
	if (rc != TSS2_RC_SUCCESS) {
		fp_error("failed to start auth session");
		return -EINVAL;
	}

	rc = Esys_TR_SetAuth(esys, ESYS_TR_RH_OWNER, &auth_value);
	if (rc != TSS2_RC_SUCCESS) {
		fp_error("failed to set auth");
		return -EINVAL;
	}

	return 0;
}

static int
tpm2_create_primary(ESYS_CONTEXT *esys, ESYS_TR session_handle, ESYS_TR *handle)
{
	TPM2B_SENSITIVE_CREATE in_sensitive = { 0 };
	TPM2B_PUBLIC in_public = {
		.size = 0,
		.publicArea = {
			.type = TPM2_ALG_ECC,
			.nameAlg = TPM2_ALG_SHA256,
			.objectAttributes = (TPMA_OBJECT_RESTRICTED |
					     TPMA_OBJECT_DECRYPT |
					     TPMA_OBJECT_FIXEDTPM |
					     TPMA_OBJECT_FIXEDPARENT |
					     TPMA_OBJECT_SENSITIVEDATAORIGIN |
					     TPMA_OBJECT_USERWITHAUTH),
			.authPolicy.size = 0,
			.parameters.eccDetail = {
				.symmetric = {
					.algorithm = TPM2_ALG_AES,
					.keyBits.aes = 128,
					.mode.aes = TPM2_ALG_CFB,
				},
				.scheme.scheme = TPM2_ALG_NULL,
				.curveID = TPM2_ECC_NIST_P256,
				.kdf.scheme = TPM2_ALG_NULL,
			},
			.unique.ecc = {
				.x = {.size = 0,.buffer = {0}},
				.y = {.size = 0,.buffer = {0}}
			},
		}
	};
	TPM2B_DATA outside_info = { 0 };
	TPML_PCR_SELECTION creation_pcr = { 0 };
	TSS2_RC rc;

	rc = Esys_CreatePrimary(esys, ESYS_TR_RH_OWNER,
				session_handle, ESYS_TR_NONE, ESYS_TR_NONE,
				&in_sensitive, &in_public,
				&outside_info, &creation_pcr, handle,
				NULL, NULL, NULL, NULL);
	if (rc != TSS2_RC_SUCCESS) {
		fp_error("failed to create primary");
		return -EINVAL;
	}

	return 0;
}

static int
tpm2_create(ESYS_CONTEXT *esys, ESYS_TR primary_handle, ESYS_TR session_handle,
	    const char *password,
	    uint8_t *pubkey, size_t *pubkey_len,
	    uint8_t *key_handle, size_t *key_handle_len)
{
	TPM2B_SENSITIVE_CREATE in_sensitive = { 0 };
	TPM2B_PUBLIC in_public = {
		.size = 0,
		.publicArea = {
			.type = TPM2_ALG_ECC,
			.nameAlg = TPM2_ALG_SHA256,
			.objectAttributes = (TPMA_OBJECT_USERWITHAUTH |
					     TPMA_OBJECT_SIGN_ENCRYPT |
					     TPMA_OBJECT_SENSITIVEDATAORIGIN),
			.authPolicy.size = 0,
			.parameters.eccDetail = {
				.symmetric.algorithm = TPM2_ALG_NULL,
				.scheme.scheme = TPM2_ALG_NULL,
				.curveID = TPM2_ECC_NIST_P256,
				.kdf.scheme = TPM2_ALG_NULL,
			},
			.unique.ecc = {
				.x = {.size = 0,.buffer = { 0 }},
				.y = {.size = 0,.buffer = { 0 }}
			},
		}
	};
	TPM2B_TEMPLATE template = { .size = 0 };
	TSS2_RC rc;
	size_t offset;
	ESYS_TR object_handle;
	_cleanup_(esys_freep) TPM2B_PRIVATE *out_private = NULL;
	_cleanup_(esys_freep) TPM2B_PUBLIC *out_public = NULL;
	uint8_t buffer[1024];

	if (password) {
		memcpy(in_sensitive.sensitive.userAuth.buffer, password, strlen(password));
		in_sensitive.sensitive.userAuth.size = strlen(password);
	}

	offset = 0;
	rc = Tss2_MU_TPMT_PUBLIC_Marshal(&in_public.publicArea,
					 &template.buffer[0],
					 sizeof(TPMT_PUBLIC),
					 &offset);
	if (rc != TSS2_RC_SUCCESS) {
		fp_error("failed to marshal key template");
		return -EINVAL;
	}
	template.size = offset;

	rc = Esys_CreateLoaded(esys, primary_handle,
			       session_handle, ESYS_TR_NONE, ESYS_TR_NONE,
			       &in_sensitive, &template,
			       &object_handle, &out_private, &out_public);
	(void)Esys_FlushContext(esys, object_handle);
	if (rc != TSS2_RC_SUCCESS) {
		fp_error("failed to create key");
		return -EINVAL;
	}

	assert(pubkey_len && *pubkey_len >= 65);

	offset = 0;
	memcpy(&pubkey[offset],
	       out_public->publicArea.unique.ecc.x.buffer,
	       out_public->publicArea.unique.ecc.x.size);
	offset += out_public->publicArea.unique.ecc.x.size;
	memcpy(&pubkey[offset],
	       out_public->publicArea.unique.ecc.y.buffer,
	       out_public->publicArea.unique.ecc.y.size);
	offset += out_public->publicArea.unique.ecc.y.size;
	*pubkey_len = offset;

	offset = 0;
	rc = Tss2_MU_TPM2B_PRIVATE_Marshal(out_private,
					   buffer,
					   sizeof(buffer),
					   &offset);
	if (rc != TSS2_RC_SUCCESS) {
		fp_error("failed to marshal private key");
		return -EINVAL;
	}
	rc = Tss2_MU_TPM2B_PUBLIC_Marshal(out_public,
					  buffer,
					  sizeof(buffer),
					  &offset);
	if (rc != TSS2_RC_SUCCESS) {
		return -EINVAL;
	}

	assert(key_handle_len && *key_handle_len >= offset);
	memcpy(key_handle, buffer, offset);
	*key_handle_len = offset;

	return 0;
}

static int
tpm2_load(ESYS_CONTEXT *esys, ESYS_TR primary_handle, ESYS_TR session_handle,
	  const uint8_t *key_handle, size_t key_handle_len,
	  ESYS_TR *handle)
{
	TPM2B_PRIVATE priv = { .size = 0 };
	TPM2B_PUBLIC pub = { .size = 0 };
	size_t offset;
	TPM2_RC rc;

	offset = 0;
	rc = Tss2_MU_TPM2B_PRIVATE_Unmarshal(key_handle,
					     key_handle_len,
					     &offset,
					     &priv);
	if (rc != TSS2_RC_SUCCESS) {
		return -EINVAL;
	}

	rc = Tss2_MU_TPM2B_PUBLIC_Unmarshal(key_handle,
					    key_handle_len,
					    &offset,
					    &pub);
	if (rc != TSS2_RC_SUCCESS) {
		return -EINVAL;
	}

	rc = Esys_Load(esys, primary_handle,
		       session_handle, ESYS_TR_NONE, ESYS_TR_NONE,
		       &priv, &pub, handle);
	if (rc != TSS2_RC_SUCCESS) {
		return -EINVAL;
	}

	return 0;
}

static int
tpm2_sign(ESYS_CONTEXT *esys, ESYS_TR object_handle, ESYS_TR session_handle,
	  const char *rp,
	  const uint8_t *client_data_hash, size_t client_data_hash_len,
	  const char *password,
	  uint8_t *signature, size_t *signature_len)
{
	size_t offset;
	gnutls_digest_algorithm_t dig = GNUTLS_DIG_SHA256;
	_cleanup_(esys_freep) TPM2B_DIGEST *digest = NULL;
	_cleanup_(esys_freep) TPMT_TK_HASHCHECK *validation = NULL;
	TPMT_SIG_SCHEME in_scheme = {
		.scheme = TPM2_ALG_ECDSA,
		.details.ecdsa.hashAlg = TPM2_ALG_SHA256,
	};
	TPM2B_MAX_BUFFER buf;
	TPMT_SIGNATURE *sig;
	TPMS_SIGNATURE_ECDSA *ecdsa;
	TPM2_RC rc;
	gnutls_datum_t data;
	gnutls_datum_t sig_r, sig_s;
	int r;

	assert(client_data_hash_len == 32);
	assert(32 + 1 + 4 + 32 <= TPM2_MAX_DIGEST_BUFFER);

	offset = 0;
	r = gnutls_hash_fast(dig, rp, strlen(rp), &buf.buffer[offset]);
	if (r < 0) {
		return -EINVAL;
	}
	offset += gnutls_hash_get_len(dig);
	buf.buffer[offset++] = 0x1;
	memset(&buf.buffer[offset], 0, 4);
	offset += 4;
	memcpy(&buf.buffer[offset], client_data_hash, client_data_hash_len);
	offset += client_data_hash_len;
	buf.size = offset;

	rc = Esys_Hash(esys,
		       ESYS_TR_NONE, ESYS_TR_NONE, ESYS_TR_NONE,
		       &buf,
		       TPM2_ALG_SHA256,
		       TPM2_RH_NULL,
		       &digest,
		       &validation);
	if (rc != TSS2_RC_SUCCESS) {
		return -EINVAL;
	}

	if (password) {
		TPM2B_AUTH auth_value;
		memcpy(auth_value.buffer, password, strlen(password));
		auth_value.size = strlen(password);
		rc = Esys_TR_SetAuth(esys, object_handle, &auth_value);
		if (rc != TSS2_RC_SUCCESS) {
			return -EINVAL;
		}
	}

	rc = Esys_Sign(esys, object_handle,
		       session_handle, ESYS_TR_NONE, ESYS_TR_NONE,
		       digest, &in_scheme, validation, &sig);
	if (rc != TSS2_RC_SUCCESS) {
		return -EINVAL;
	}

	ecdsa = &sig->signature.ecdsa;
	sig_r.data = ecdsa->signatureR.buffer;
	sig_r.size = ecdsa->signatureR.size;
	sig_s.data = ecdsa->signatureS.buffer;
	sig_s.size = ecdsa->signatureS.size;
	gnutls_encode_rs_value(&data, &sig_r, &sig_s);

	if (data.size > *signature_len) {
		gnutls_free(data.data);
		return -EINVAL;
	}
	memcpy(signature, data.data, data.size);
	*signature_len = data.size;
	gnutls_free(data.data);

	return 0;
}

static inline void
gnutls_pubkey_deinitp(gnutls_pubkey_t *pubkey)
{
	gnutls_pubkey_deinit(*pubkey);
}

static int
get_dig(gnutls_x509_crt_t crt)
{
	gnutls_digest_algorithm_t dig;
	_cleanup_(gnutls_pubkey_deinitp) gnutls_pubkey_t pubkey = NULL;
	unsigned int mand;
	int r;

	r = gnutls_pubkey_init(&pubkey);
	if (r < 0) {
		return r;
	}

	r = gnutls_pubkey_import_x509(pubkey, crt, 0);
	if (r < 0) {
		return r;
	}

	r = gnutls_pubkey_get_preferred_hash_algorithm(pubkey, &dig, &mand);
	if (r < 0) {
		return r;
	}

	return dig;
}

static int
server_sign_attestation(struct fp_server *server,
			const char *rp,
			const uint8_t *client_data_hash,
			size_t client_data_hash_len,
			const uint8_t *key_handle,
			size_t key_handle_len,
			const uint8_t *pubkey,
			size_t pubkey_len,
			uint8_t *signature,
			size_t *signature_len)
{
	_cleanup_free_ uint8_t *buffer = NULL;
	size_t offset;
	int r;
	gnutls_digest_algorithm_t dig = GNUTLS_DIG_SHA256;
	gnutls_datum_t data;

	buffer = malloc(1 + gnutls_hash_get_len(dig) + client_data_hash_len + key_handle_len + 1 + pubkey_len);
	if (!buffer) {
		return -ENOMEM;
	}

	offset = 0;
	buffer[offset++] = 0;
	r = gnutls_hash_fast(dig, rp, strlen(rp), &buffer[offset]);
	if (r < 0) {
		return -EINVAL;
	}
	offset += gnutls_hash_get_len(dig);

	memcpy(&buffer[offset], client_data_hash, client_data_hash_len);
	offset += client_data_hash_len;

	memcpy(&buffer[offset], key_handle, key_handle_len);
	offset += key_handle_len;

	/* uncompressed */
	buffer[offset++] = 0x4;
	memcpy(&buffer[offset], pubkey, pubkey_len);
	offset += pubkey_len;

	data.data = buffer;
	data.size = offset;
	r = gnutls_x509_privkey_sign_data(server->att_privkey,
					  get_dig(server->att_cert), 0,
					  &data, signature, signature_len);
	if (r < 0) {
		return -EINVAL;
	}

	return 0;
}

static int
request_make_cred(sd_bus *bus, const char *object_path, void *data)
{
	struct closure *closure = data;
	struct fp_server *server = closure->server;
	_cleanup_(sd_bus_message_unrefp) sd_bus_message *m = NULL;
	ESYS_TR primary_handle, session_handle;
	uint8_t pubkey[65];
	size_t pubkey_len = sizeof(pubkey);
	uint8_t key_handle[512];
	size_t key_handle_len = sizeof(key_handle);
	uint8_t signature[512];
	size_t signature_len = sizeof(signature);
	gnutls_datum_t x5c = { NULL, 0 };
	g_autoptr(GcrSystemPrompt) prompt = NULL;
	GError *error;
	const gchar *password;
	gchar *text;
	int r;

	if (closure->cancelled) {
		r = new_error_signal(bus, &m, object_path,
				     FP_ERROR_CANCELLED,
				     "The request was cancelled");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		return sd_bus_send(bus, m, NULL);
	}

	r = tpm2_start_auth_session(server->esys, &session_handle);
	if (r < 0) {
		r = new_error_signal(bus, &m, object_path,
				     FP_ERROR_PLATFORM_ERROR,
				     "Failed to make credential");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		return sd_bus_send(bus, m, NULL);
	}

	r = tpm2_create_primary(server->esys, session_handle, &primary_handle);
	if (r < 0) {
		(void)Esys_FlushContext(server->esys, session_handle);
		r = new_error_signal(bus, &m, object_path,
				     FP_ERROR_PLATFORM_ERROR,
				     "Failed to make credential");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		return sd_bus_send(bus, m, NULL);
	}

	error = NULL;
	prompt = GCR_SYSTEM_PROMPT(gcr_system_prompt_open(-1, NULL, &error));
	if (!prompt) {
		fp_error("failed to open prompt: %s\n", error->message);
		g_clear_error(&error);
		r = new_error_signal(bus, &m, object_path,
				     FP_ERROR_PLATFORM_ERROR,
				     "Failed to make credential");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		return sd_bus_send(bus, m, NULL);
	}

	gcr_prompt_set_message(GCR_PROMPT(prompt), "Password for TPM credential");
	text = g_strdup_printf("Enter new password for %s", closure->rp);
	gcr_prompt_set_description(GCR_PROMPT(prompt), text);
	g_free(text);

	error = NULL;
	password = gcr_prompt_password(GCR_PROMPT(prompt), NULL, &error);
	if (error) {
		fp_error("failed to prompt password: %s\n", error->message);
		g_clear_error(&error);
		r = new_error_signal(bus, &m, object_path,
				     FP_ERROR_PLATFORM_ERROR,
				     "Failed to make credential");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		return sd_bus_send(bus, m, NULL);
	}

	r = tpm2_create(server->esys, primary_handle, session_handle,
			password,
			pubkey, &pubkey_len, key_handle, &key_handle_len);
	(void)Esys_FlushContext(server->esys, primary_handle);
	(void)Esys_FlushContext(server->esys, session_handle);
	if (r < 0) {
		r = new_error_signal(bus, &m, object_path,
				     FP_ERROR_PLATFORM_ERROR,
				     "Failed to make credential");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		return sd_bus_send(bus, m, NULL);
	}

	r = sd_bus_message_new_signal(bus, &m, object_path,
				      FP_REQUEST_INTERFACE,
				      "Completed");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = sd_bus_message_open_container(m, SD_BUS_TYPE_ARRAY, "{sv}");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = append_bytearray_entry(m, "publicKey",
				   pubkey,
				   pubkey_len);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = append_bytearray_entry(m, "credentialID",
				   key_handle,
				   key_handle_len);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = gnutls_x509_crt_export2(server->att_cert, GNUTLS_X509_FMT_DER, &x5c);
	if (r < 0) {
		fp_error("failed to export certificate: %s",
			 gnutls_strerror(r));
		return -EINVAL;
	}
	r = append_bytearray_entry(m, "x5c", x5c.data, x5c.size);
	gnutls_free(x5c.data);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = server_sign_attestation(server,
				    closure->rp,
				    closure->client_data_hash,
				    closure->client_data_hash_len,
				    key_handle, key_handle_len,
				    pubkey, pubkey_len,
				    signature, &signature_len);
	if (r < 0) {
		return r;
	}

	r = append_bytearray_entry(m, "signature", signature, signature_len);
	if (r < 0) {
		fp_log_errno(r);
		return r;
	}
	r = sd_bus_message_append(m, "{sv}", "fmt", "s", "fido-u2f");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = sd_bus_message_close_container(m);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	return sd_bus_send(bus, m, NULL);
}

static int
request_get_assertion(sd_bus *bus, const char *object_path, void *data)
{
	struct closure *closure = data;
	struct fp_server *server = closure->server;
	_cleanup_(sd_bus_message_unrefp) sd_bus_message *m = NULL;
	ESYS_TR primary_handle, session_handle, object_handle;
	uint8_t signature[512];
	size_t signature_len = sizeof(signature);
	g_autoptr(GcrSystemPrompt) prompt = NULL;
	GError *error;
	const gchar *password;
	gchar *text;
	int r;

	if (closure->cancelled) {
		r = new_error_signal(bus, &m, object_path,
				     FP_ERROR_CANCELLED,
				     "The request was cancelled");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}

		return sd_bus_send(bus, m, NULL);
	}

	r = tpm2_start_auth_session(server->esys, &session_handle);
	if (r < 0) {
		r = new_error_signal(bus, &m, object_path,
				     FP_ERROR_PLATFORM_ERROR,
				     "Failed to get assertion");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		return sd_bus_send(bus, m, NULL);
	}

	r = tpm2_create_primary(server->esys, session_handle, &primary_handle);
	if (r < 0) {
		(void)Esys_FlushContext(server->esys, session_handle);
		r = new_error_signal(bus, &m, object_path,
				     FP_ERROR_PLATFORM_ERROR,
				     "Failed to get assertion");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		return sd_bus_send(bus, m, NULL);
	}

	r = tpm2_load(server->esys, primary_handle, session_handle,
		      closure->key_handle, closure->key_handle_len,
		      &object_handle);
	if (r < 0) {
		(void)Esys_FlushContext(server->esys, primary_handle);
		(void)Esys_FlushContext(server->esys, session_handle);
		r = new_error_signal(bus, &m, object_path,
				     FP_ERROR_PLATFORM_ERROR,
				     "Failed to get assertion");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		return sd_bus_send(bus, m, NULL);
	}

	error = NULL;
	prompt = GCR_SYSTEM_PROMPT(gcr_system_prompt_open(-1, NULL, &error));
	if (!prompt) {
		fp_error("failed to open prompt: %s\n", error->message);
		g_clear_error(&error);
		r = new_error_signal(bus, &m, object_path,
				     FP_ERROR_PLATFORM_ERROR,
				     "Failed to make credential");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		return sd_bus_send(bus, m, NULL);
	}

	gcr_prompt_set_message(GCR_PROMPT(prompt), "Password for TPM credential");
	text = g_strdup_printf("Enter password for %s (0x%x)",
			       closure->rp, object_handle);
	gcr_prompt_set_description(GCR_PROMPT(prompt), text);
	g_free(text);

	error = NULL;
	password = gcr_prompt_password(GCR_PROMPT(prompt), NULL, &error);
	if (error) {
		fp_error("failed to prompt password: %s\n", error->message);
		g_clear_error(&error);
		r = new_error_signal(bus, &m, object_path,
				     FP_ERROR_PLATFORM_ERROR,
				     "Failed to make credential");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		return sd_bus_send(bus, m, NULL);
	}

	r = tpm2_sign(server->esys, object_handle, session_handle,
		      closure->rp,
		      closure->client_data_hash,
		      closure->client_data_hash_len,
		      password,
		      signature, &signature_len);
	(void)Esys_FlushContext(server->esys, primary_handle);
	(void)Esys_FlushContext(server->esys, session_handle);
	(void)Esys_FlushContext(server->esys, object_handle);
	if (r < 0) {
		r = new_error_signal(bus, &m, object_path,
				     FP_ERROR_PLATFORM_ERROR,
				     "Failed to get assertion");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		return sd_bus_send(bus, m, NULL);
	}

	r = sd_bus_message_new_signal(bus, &m, object_path,
				      FP_REQUEST_INTERFACE,
				      "Completed");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = sd_bus_message_open_container(m, SD_BUS_TYPE_ARRAY, "{sv}");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = append_bytearray_entry(m, "clientdataHash",
				   closure->client_data_hash,
				   closure->client_data_hash_len);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = sd_bus_message_append(m, "s", "assertions");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "a{sv}");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = sd_bus_message_open_container(m, SD_BUS_TYPE_ARRAY, "{sv}");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_append(m, "{sv}", "count", "t", 0);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = append_bytearray_entry(m, "signature", signature, signature_len);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = sd_bus_message_append(m, "{sv}", "sigCount", "u", 0);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_close_container(m); /* array of {sv} */
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = sd_bus_message_close_container(m); /* variant of a{sv} */
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = sd_bus_message_close_container(m); /* dict entry of {"assertions", v} */
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = sd_bus_message_close_container(m); /* array of {sv} */
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	return sd_bus_send(bus, m, NULL);
}

static int
handle_make_credential(sd_bus_message *m, void *userdata,
		       sd_bus_error *ret_error)
{
	int r;
	const char *type;
	uint8_t *client_data_hash;
	size_t client_data_hash_len;
	const char *rp;
	struct fp_device *device = userdata;
	int force_fido2 = false;
	int force_u2f = false;
	_cleanup_free_ char *request_object_path = NULL;
	struct closure *closure;

	r = sd_bus_message_read_basic(m, 's', &type);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	if (!streq(type, "es256")) {
		fp_error("unknown type: \"%s\"", type);
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Unknown type: \"%s\"",
						  type);
	}

	r = sd_bus_message_read_array(m, 'y',
				      (const void **)&client_data_hash,
				      &client_data_hash_len);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_read_basic(m, 's', &rp);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_skip(m, "ays");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_enter_container(m, 'a', "{sv}");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	for (;;) {
		const char *label;

		r = sd_bus_message_enter_container(m, 'e', "sv");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		if (r == 0){
			break;
		}

		r = sd_bus_message_read_basic(m, 's', &label);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}

		if (streq(label, "forceFIDO2")) {
			r = sd_bus_message_read(m, "v", "b", &force_fido2);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

		} else if (streq(label, "forceU2F")) {
			r = sd_bus_message_read(m, "v", "b", &force_u2f);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

		} else {
			r = sd_bus_message_skip(m, "v");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
		}

		r = sd_bus_message_exit_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
	}

	r = sd_bus_message_exit_container(m);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	if (force_fido2 && force_u2f) {
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Can't force FIDO2 and U2F at the same time");
	}

	closure = closure_new(device->server, rp,
			      client_data_hash, client_data_hash_len);
	if (!closure) {
		fp_error("failed to allocate closure");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	if (asprintf(&request_object_path, FP_REQUEST_PATH "/%lu",
		     device->server->requests.n_requests) < 0) {
		fp_error("failed to allocate request_object_path");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	r = fp_server_enqueue_request(device->server, request_object_path,
				      request_make_cred, closure, closure_free);
	if (r < 0) {
		fp_error("failed to enqueue request");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	return sd_bus_reply_method_return(m, "o", request_object_path);
}

static int
handle_get_assertion(sd_bus_message *m, void *userdata, sd_bus_error *ret_error)
{
	int r;
	uint8_t *client_data_hash;
	size_t client_data_hash_len;
	const char *rp;
	uint8_t *key_handle;
	size_t key_handle_len;
	int uv;
	struct fp_device *device = userdata;
	_cleanup_free_ char *request_object_path = NULL;
	struct closure *closure;

	r = sd_bus_message_read_array(m, 'y',
				      (const void **)&client_data_hash,
				      &client_data_hash_len);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_read_basic(m, 's', &rp);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_read_array(m, 'y',
				      (const void **)&key_handle,
				      &key_handle_len);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_enter_container(m, 'a', "{sv}");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	for (;;) {
		const char *label;

		r = sd_bus_message_enter_container(m, 'e', "sv");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		if (r == 0) {
			break;
		}

		r = sd_bus_message_read_basic(m, 's', &label);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}

		if (streq(label, "uv")) {
			r = sd_bus_message_read(m, "v", "b", &uv);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
		} else {
			r = sd_bus_message_skip(m, "v");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
		}

		r = sd_bus_message_exit_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
	}

	r = sd_bus_message_exit_container(m);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	closure = closure_new(device->server, rp,
			      client_data_hash, client_data_hash_len);
	closure->key_handle = memdup(key_handle, key_handle_len);
	if (!closure->key_handle) {
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}
	closure->key_handle_len = key_handle_len;
	closure->uv = uv;

	if (asprintf(&request_object_path, FP_REQUEST_PATH "/%lu",
		     device->server->requests.n_requests) < 0) {
		fp_error("failed to allocate request_object_path");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	r = fp_server_enqueue_request(device->server, request_object_path,
				      request_get_assertion, closure,
				      closure_free);
	if (r < 0) {
		fp_error("failed to enqueue request");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	return sd_bus_reply_method_return(m, "o", request_object_path);
}

static const sd_bus_vtable device_vtable[] = {
	SD_BUS_VTABLE_START(0),
	SD_BUS_PROPERTY("Path", "s", NULL, offsetof(struct fp_device, path),
			SD_BUS_VTABLE_PROPERTY_CONST),
	SD_BUS_METHOD_WITH_NAMES("MakeCredential", "saysaysa{sv}", SD_BUS_PARAM(type) SD_BUS_PARAM(client_data_hash) SD_BUS_PARAM(rp) SD_BUS_PARAM(user_id) SD_BUS_PARAM(user_name) SD_BUS_PARAM(options), "o", SD_BUS_PARAM(handle),
				 handle_make_credential, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_METHOD_WITH_NAMES("GetAssertion",
				 "aysaya{sv}", SD_BUS_PARAM(client_data_hash) SD_BUS_PARAM(rp) SD_BUS_PARAM(allow_cred) SD_BUS_PARAM(options),
				 "o", SD_BUS_PARAM(handle),
				 handle_get_assertion, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_VTABLE_END
};

static int
server_load_attestation(struct fp_server *server)
{
	int r = 0;
	gnutls_datum_t cert_data = { NULL, 0 };
	gnutls_datum_t privkey_data = { NULL, 0 };

	r = gnutls_x509_crt_init(&server->att_cert);
	if (r < 0) {
		fp_error("failed to allocate certificate");
		r = -ENOMEM;
		goto out;
	}

	r = gnutls_load_file(att_cert_file, &cert_data);
	if (r < 0) {
		fp_error("failed to load certificate file");
		r = -EINVAL;
		goto out;
	}

	r = gnutls_x509_crt_import(server->att_cert, &cert_data, GNUTLS_X509_FMT_PEM);
	if (r < 0) {
		fp_error("failed to import certificate");
		r = -EINVAL;
		goto out;
	}

	r = gnutls_x509_privkey_init(&server->att_privkey);
	if (r < 0) {
		fp_error("failed to allocate private key");
		r = -ENOMEM;
		goto out;
	}

	r = gnutls_load_file(att_privkey_file, &privkey_data);
	if (r < 0) {
		fp_error("failed to load private key file");
		r = -EINVAL;
		goto out;
	}

	r = gnutls_x509_privkey_import2(server->att_privkey, &privkey_data,
					GNUTLS_X509_FMT_PEM, NULL, 0);
	if (r < 0) {
		fp_error("failed to import private key");
		r = -EINVAL;
		goto out;
	}

 out:
	gnutls_free(cert_data.data);
	gnutls_free(privkey_data.data);
	if (r < 0) {
		if (server->att_cert) {
			gnutls_x509_crt_deinit(server->att_cert);
		}
		if (server->att_privkey) {
			gnutls_x509_privkey_deinit(server->att_privkey);
		}
	}
	return r;
}

static int
server_startup(struct fp_server *server)
{
	TSS2_RC rc;
	int r;

	rc = Tss2_TctiLdr_Initialize(NULL, &server->tcti);
	if (rc != TSS2_RC_SUCCESS) {
		return -EINVAL;
	}

	rc = Esys_Initialize(&server->esys, server->tcti, NULL);
	if (rc != TSS2_RC_SUCCESS) {
		return -EINVAL;
	}

	rc = Esys_Startup(server->esys, TPM2_SU_CLEAR);
	if (rc != TSS2_RC_SUCCESS) {
		return -EINVAL;
	}

	r = server_load_attestation(server);
	if (r < 0) {
		return -EINVAL;
	}

	return 0;
}

static void
server_teardown(struct fp_server *server)
{
	Esys_Finalize(&server->esys);
	Tss2_TctiLdr_Finalize(&server->tcti);
	gnutls_x509_crt_deinit(server->att_cert);
	gnutls_x509_privkey_deinit(server->att_privkey);
}

static int
server_enumerate_devices(struct fp_server *server)
{
	_cleanup_(fp_device_freep) struct fp_device *device = NULL;
	int r;

	device = calloc(1, sizeof(struct fp_device));
	if (!device)
		return -ENOMEM;

	device->server = server;

	if (asprintf(&device->object_path, FP_DEVICE_PATH "/%lu",
		     server->n_devices) < 0)
		return -ENOMEM;

	r = sd_bus_add_object_vtable(server->bus, &device->slot,
				     device->object_path,
				     FP_DEVICE_INTERFACE,
				     device_vtable, device);
	if (r < 0)
		return r;

	device->next = NULL;
	server->devices = device;
	device = NULL;
	server->n_devices = 1;

	return 0;
}

static int
handle_cancel(sd_bus_message *m, void *_closure)
{
	struct closure *closure = _closure;

	if (closure->cancelled) {
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Operation already cancelled");
	}
	closure->cancelled = true;

	return sd_bus_reply_method_return(m, NULL, NULL);
}

static int
global_init(bool debug, const char *certfile, const char *keyfile)
{
	if (!certfile || !keyfile) {
		fp_error("certificate and key are required for TPM2");
		return -EINVAL;
	}

	att_cert_file = certfile;
	att_privkey_file = keyfile;

	return 0;
}

struct fp_backend fp_tpm2_backend = {
	.server_startup = server_startup,
	.server_teardown = server_teardown,
	.server_enumerate_devices = server_enumerate_devices,
	.handle_cancel = handle_cancel,
	.global_init = global_init,
};
