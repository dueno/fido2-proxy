<?xml version="1.0"?>
<!--
SPDX-License-Identifier: LGPL-3.0-or-later OR GPL-2.0

Copyright (C) 2020 Red Hat, Inc.

Author: Norbert Pocs

This file is part of fido2-proxy.

fido2-proxy is free software: you can redistribute it and/or
modify it under the terms of either:

  * the GNU Lesser General Public License as published by the Free
    Software Foundation; either version 3 of the License, or (at your
    option) any later version.

or

  * the GNU General Public License as published by the Free
    Software Foundation; either version 2 of the License, or (at your
    option) any later version.

or both in parallel, as here.

fido2-proxy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received copies of the GNU General Public License and
the GNU Lesser General Public License along with this program.  If
not, see https://www.gnu.org/licenses/.
-->
<node name="/" xmlns:doc="http://www.freedesktop.org/dbus/1.0/doc.dtd">
  <!--
      org.freedesktop.fido2.Device:
      @short_description: Authenticator device interface

      This interface provides access to a FIDO2 device.

      This documentation describes version 1 of this interface.
  -->
  <interface name="org.freedesktop.fido2.Device">

    <!--
	MakeCredential:
	@type: Name of the signing algorithm.
	@client_data_hash: Hash of the serialized client data collected by the host.
	@rp: Identifier of relying party
	@user: Identifier of user account
	@options: Vardict with optional further information
        @handle: Object path to export the Request object at

        Request generation of a new credential in the authenticator device.

        Supported keys in the @options include:
        <variablelist>
          <varlistentry>
            <term>rp_label s</term>
            <listitem><para>Label for @rp.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>user_label s</term>
            <listitem><para>Label for @user.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>rk b</term>
            <listitem><para>Whether to instruct the authenticator to store the key material on the device. Default depends on authenticator.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>uv b</term>
            <listitem><para>Whether to require a gesture that verifies the user to complete the request. Default depends on authenticator.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>forceFIDO2 b</term>
            <listitem><para>Whether to force the device to use CTAP2. Default is false.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>forceU2F b</term>
            <listitem><para>Whether to force the device to use CTAP1. Default is false.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>pinFD h</term>
            <listitem><para>The file descriptor where the PIN is read from.</para></listitem>
          </varlistentry>
        </variablelist>

        When the request completes successfully the following fields
        will be set in the Complete signal:
        <variablelist>
          <varlistentry>
            <term>authData ay</term>
            <listitem><para>The authentication data.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>signature ay</term>
            <listitem><para>The signature.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>credentialID ay</term>
            <listitem><para>The credential ID.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>publicKey ay</term>
            <listitem><para>The user public key.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>x5c ay</term>
            <listitem><para>The attestation certificate in the X.509 format.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>fmt s</term>
            <listitem><para>Format of the response.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>clientdataHash ay</term>
            <listitem><para>The hash of the clientData.</para></listitem>
          </varlistentry>
        </variablelist>
    -->
    <method name='MakeCredential'>
      <arg name='type' type='s'/>
      <arg name='client_data_hash' type='ay'/>
      <arg name='rp' type='s'/>
      <arg name='user' type='ay'/>
      <arg name='options' type='a{sv}'/>
      <arg name='handle' direction='out' type='o'/>
    </method>

    <!--
	GetAssertion:
	@client_data_hash: Hash of the serialized client data collected by the host.
	@rp: Identifier of relying party
	@options: Vardict with optional further information
        @handle: Object path to export the Request object at

        Request cryptographic proof of user authentication as well as
        user consent to a given transaction, using a previously
        generated credential that is bound to the authenticator and
        relying party identifier.

        Supported keys in the @options include:
        <variablelist>
          <varlistentry>
            <term>up b</term>
            <listitem><para>Whether to require user consent to complete the operation. Default depends on authenticator.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>uv b</term>
            <listitem><para>Whether to require a gesture that verifies the user to complete the request. Default depends on authenticator.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>forceFIDO2 b</term>
            <listitem><para>Whether to force the device to use CTAP2. Default is false.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>forceU2F b</term>
            <listitem><para>Whether to force the device to use CTAP1. Default is false.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>pinFD h</term>
            <listitem><para>The file descriptor where the PIN is read from.</para></listitem>
          </varlistentry>
        </variablelist>

        When the request completes successfully the following fields
        will be set in the Complete signal:
        <variablelist>
          <varlistentry>
            <term>clientdataHash ay</term>
            <listitem><para>The hash of the clientData.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>assertions a{sv}</term>
            <listitem><para>The array of assertions.</para></listitem>
          </varlistentry>
        </variablelist>

	Each assertion includes the following fields:
        <variablelist>
          <varlistentry>
            <term>authData ay</term>
            <listitem><para>The authentication data.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>signature ay</term>
            <listitem><para>The signature.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>sigCount u</term>
            <listitem><para>The counter value incremented upon signing.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>userID ay</term>
            <listitem><para>The user ID.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>userName s</term>
            <listitem><para>The user name.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>userDisplayName s</term>
            <listitem><para>The user display name.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>userIcon s</term>
            <listitem><para>The user icon.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>hmacSecret ay</term>
            <listitem><para>The HMAC secret if rk is enabled.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>count t</term>
            <listitem><para>The index of this entry in the dict.</para></listitem>
          </varlistentry>
        </variablelist>
    -->
    <method name='GetAssertion'>
      <arg name='client_data_hash' type='ay'/>
      <arg name='rp' type='s'/>
      <arg name='options' type='a{sv}'/>
      <arg name='handle' direction='out' type='o'/>
    </method>

    <property name="Path" type="u" access="read"/>
    <property name="Version" type="u" access="read"/>
  </interface>
</node>
