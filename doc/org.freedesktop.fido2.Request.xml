<?xml version="1.0"?>
<!--
SPDX-License-Identifier: LGPL-3.0-or-later OR GPL-2.0

Copyright (C) 2020 Red Hat, Inc.

Author: Norbert Pocs

This file is part of fido2-proxy.

fido2-proxy is free software: you can redistribute it and/or
modify it under the terms of either:

  * the GNU Lesser General Public License as published by the Free
    Software Foundation; either version 3 of the License, or (at your
    option) any later version.

or

  * the GNU General Public License as published by the Free
    Software Foundation; either version 2 of the License, or (at your
    option) any later version.

or both in parallel, as here.

fido2-proxy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received copies of the GNU General Public License and
the GNU Lesser General Public License along with this program.  If
not, see https://www.gnu.org/licenses/.
-->
<node name="/" xmlns:doc="http://www.freedesktop.org/dbus/1.0/doc.dtd">
  <!--
      org.freedesktop.fido2.Request:
      @short_description: Request interface

      This interface provides asynchronous operations on the authentication device.

      This documentation describes version 1 of this interface.
  -->
  <interface name="org.freedesktop.fido2.Request">

    <!--
	Cancel:

        Cancel the pending request.
    -->
    <method name='Cancel'>
    </method>

    <!--
	Error:
	@code: The error code.
	@cause: The error description.

        Emitted when the request fails.  The following error codes are
        defined:
        <variablelist>
          <varlistentry>
            <term>0</term>
            <listitem><para>Unknown error.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>1</term>
            <listitem><para>The operation was cancelled.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>2</term>
            <listitem><para>Invalid request.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>3</term>
            <listitem><para>PIN required.</para></listitem>
          </varlistentry>
          <varlistentry>
            <term>4</term>
            <listitem><para>Platform error.</para></listitem>
          </varlistentry>
        </variablelist>
    -->
    <signal name='Error'>
      <arg name='code' direction='out' type='u'/>
      <arg name='cause' direction='out' type='s'/>
    </signal>

    <!--
	Completed:
	@result: Vardict with the results of the request.

        Emitted when the request has completed
    -->
    <signal name='Completed'>
      <arg name='result' direction='out' type='a{sv}'/>
    </signal>

    <property name="Version" type="u" access="read"/>
  </interface>
</node>
