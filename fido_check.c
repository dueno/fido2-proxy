/*
 * Copyright (c) 2018 Yubico AB. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "config.h"

#include "fido_check.h"

#include <linux/hidraw.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

static int
get_key_val(const void *body, size_t key_len, uint32_t *val)
{
	const uint8_t *ptr = body;

	switch(key_len){
	case 0:
		*val = 0;
		break;
	case 1:
		*val = ptr[0];
		break;
	case 2:
		*val = (uint32_t)((ptr[1] << 8) | ptr[0]);
		break;
	default:
		return -1;
	}

	return 0;
}

static int
get_key_len(uint8_t tag, uint8_t *key, size_t *key_len)
{
	*key = tag & 0xfc;
	if ((*key & 0xf0) == 0xf0)
		return -1;

	*key_len = tag & 0x3;
	if (*key_len == 3)
		*key_len = 4;

	return 0;
}

static int
get_usage_info(const struct hidraw_report_descriptor *hrd, uint32_t *usage_page,
	       uint32_t *usage)
{
	const uint8_t *ptr;
	size_t len;

	ptr = hrd->value;
	len = hrd->size;

	while (len > 0) {
		const uint8_t tag = ptr[0];
		ptr++;
		len--;

		uint8_t key;
		size_t key_len;
		uint32_t key_val;

		if (get_key_len(tag, &key, &key_len) < 0 || key_len > len ||
		    get_key_val(ptr, key_len, &key_val) < 0)
			return -1;

		if (key == 0x4)
			*usage_page = key_val;
		else if (key == 0x8)
			*usage = key_val;

		ptr += key_len;
		len -= key_len;
	}

	return 0;
}

#define _cleanup_(f) __attribute__((cleanup(f)))

static inline void
closep(void *p)
{
	int fd = *(int*) p;
	if (fd >= 0)
		close(fd);
}

#define _cleanup_close_ _cleanup_(closep)

static int
get_report_descriptor(const char *path, struct hidraw_report_descriptor *hrd)
{
	_cleanup_close_ int fd = -1;
	int s = -1;
	int r;

	r = open(path, O_RDONLY);
	if (r < 0)
		return r;

	fd = r;

	r = ioctl(fd, HIDIOCGRDESCSIZE, &s);
	if (r < 0)
		return r;

	if (s < 0 || s > HID_MAX_DESCRIPTOR_SIZE)
		return -1;

	hrd->size = s;

	r = ioctl(fd, HIDIOCGRDESC, hrd);
	if(r < 0)
		return r;

	return 0;
}

bool
is_fido(const char *path)
{
	uint32_t usage = 0;
	uint32_t usage_page = 0;
	struct hidraw_report_descriptor hrd;

	memset(&hrd, 0, sizeof(hrd));

	if (get_report_descriptor(path, &hrd) < 0 ||
	    get_usage_info(&hrd, &usage_page, &usage) < 0)
		return false;

	return usage_page == 0xf1d0;
}
