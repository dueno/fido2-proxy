/* SPDX-License-Identifier: LGPL-3.0-or-later OR GPL-2.0 */

/*
 * Copyright (C) 2020 Red Hat, Inc.
 *
 * This file is part of fido2-proxy.
 *
 * fido2-proxy is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * fido2-proxy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see https://www.gnu.org/licenses/.
 */

#include "config.h"

#include "log.h"
#include "server.h"

int
new_error_signal(sd_bus *bus, sd_bus_message **message, const char *object_path,
		 uint32_t code, const char *cause)
{
	_cleanup_(sd_bus_message_unrefp) sd_bus_message *m = NULL;
	int r;

	r = sd_bus_message_new_signal(bus, &m, object_path,
				      FP_REQUEST_INTERFACE,
				      "Error");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = sd_bus_message_append(m, "u", code);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = sd_bus_message_append(m, "s", message);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	*message = m;
	m = NULL;
	return 0;
}

int
append_bytearray_entry(sd_bus_message *m, const char *name,
		       const uint8_t *ptr, size_t len)
{
	int r;

	r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = sd_bus_message_append(m, "s", name);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "ay");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = sd_bus_message_append_array(m, 'y', ptr, len);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = sd_bus_message_close_container(m);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	r = sd_bus_message_close_container(m);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}
	return 0;
}
