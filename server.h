/*
 * Copyright (C) 2020 Red Hat, Inc.
 *
 * This file is part of fido2-proxy.
 *
 * fido2-proxy is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * fido2-proxy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see https://www.gnu.org/licenses/.
 */

#ifndef FP_SERVER_H_
#define FP_SERVER_H_

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <systemd/sd-bus.h>
#if WITH_TPM2
#include <gnutls/crypto.h>
#include <tss2/tss2_tctildr.h>
#include <tss2/tss2_esys.h>
#endif

#define streq(a, b) (strcmp((a), (b)) == 0)

#define _cleanup_(f) __attribute__((cleanup(f)))
static inline void freep(void *p) {
        free(*(void**) p);
}

#define _cleanup_free_ _cleanup_(freep)

enum {
	FP_ERROR_UNKNOWN_ERROR = 0,
	FP_ERROR_CANCELLED = 1,
	FP_ERROR_INVALID_REQUEST = 2,
	FP_ERROR_PIN_REQUIRED = 3,
	FP_ERROR_PLATFORM_ERROR = 4,
};

#define FP_SERVICE_NAME "org.freedesktop.fido2"
#define FP_REQUEST_PATH "/org/freedesktop/fido2/Request"
#define FP_REQUEST_INTERFACE "org.freedesktop.fido2.Request"

#define FP_DEVICE_PATH "/org/freedesktop/fido2/Device"
#define FP_DEVICE_INTERFACE "org.freedesktop.fido2.Device"

struct fp_device;

struct fp_requests {
	struct fp_request *head;
	struct fp_request *tail;
	size_t n_requests;
};

struct fp_server {
	sd_bus *bus;
	sd_bus_slot *slot;
	struct fp_device *devices;
	struct fp_requests requests;
	size_t n_devices;
	struct udev *udev;
	struct udev_monitor *udev_monitor;
	sd_event_source *udev_monitor_source;
	sd_event *event;
#if WITH_TPM2
	ESYS_CONTEXT *esys;
	TSS2_TCTI_CONTEXT *tcti;
	gnutls_x509_crt_t att_cert;
	gnutls_x509_privkey_t att_privkey;
#endif
};

struct fp_device {
	struct fp_server *server;
	sd_bus_slot *slot;
	char *path;
	char *object_path;
	struct fp_device *prev;
	struct fp_device *next;
};

/* server.c */
void fp_device_free(struct fp_device *device);

static inline void fp_device_freep(void *p) {
	fp_device_free(*(struct fp_device **)p);
}

int fp_server_enqueue_request(struct fp_server *server,
			      const char *request_object_path,
			      int (*dispatch)(sd_bus *, const char *, void *),
			      void *closure, void (*closure_free)(void *));
void lock_worker(void);
void unlock_worker(void);

struct fp_backend {
	int (*server_startup)(struct fp_server *);
	void (*server_teardown)(struct fp_server *);
	int (*server_enumerate_devices)(struct fp_server *);
	int (*handle_cancel)(sd_bus_message *, void *);
	int (*global_init)(bool, const char *, const char *);
};

/* libfido2.c */
extern struct fp_backend fp_libfido2_backend;

/* tpm2.c */
extern struct fp_backend fp_tpm2_backend;

/* utils.c */
int new_error_signal(sd_bus *bus, sd_bus_message **message,
		     const char *object_path, uint32_t code, const char *cause);
int append_bytearray_entry(sd_bus_message *m, const char *name,
			   const uint8_t *ptr, size_t len);

#endif /* FP_SERVER_H_ */
