/* SPDX-License-Identifier: LGPL-3.0-or-later OR GPL-2.0 */

/*
 * Copyright (C) 2020 Red Hat, Inc.
 *
 * This file is part of fido2-proxy.
 *
 * fido2-proxy is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * fido2-proxy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see https://www.gnu.org/licenses/.
 */

#pragma once

#include <systemd/sd-bus.h>

#include "libfido2-proxy/types.h"
#include "libfido2-proxy/err.h"

#define F2P_SERVER_DBUS_NAME			"org.freedesktop.fido2"
#define F2P_SERVER_DBUS_DEVICE_INTERFACE 	"org.freedesktop.fido2.Device"
#define F2P_SERVER_DBUS_REQUEST_INTERFACE 	"org.freedesktop.fido2.Request"
#define F2P_SERVER_DBUS_DEVICE_OBJECT 		"/org/freedesktop/fido2/Device/0"
#define F2P_SERVER_DBUS_REQUEST_OBJECT 		"/org/freedesktop/fido2/Request/0"
#define F2P_SERVER_DBUS_DEVICE_PATH 		"/org/freedesktop/fido2/Device"
#define F2P_SERVER_DBUS_REQUEST_PATH 		"/org/freedesktop/fido2/Request"
#define F2P_SERVER_MEMBER_ERR			"Error"
#define F2P_SERVER_MEMBER_OK			"Completed"
#define F2P_SERVER_MEMBER_PIN			"PinRequired"

#define F2P_ARG_AUTHDATA 			"authdata"
#define F2P_ARG_AUTHDATA_S			"authData"
#define F2P_ARG_AUTHDATA_RAW 			"authdataRaw"
#define F2P_ARG_X5C 				"x5c"
#define F2P_ARG_SIGNATURE 			"signature"
#define F2P_ARG_EXTENSIONS 			"extensions"
#define F2P_ARG_PROTECTION 			"protection"
#define F2P_ARG_RK 				"rk"
#define F2P_ARG_UV 				"uv"
#define F2P_ARG_FMT 				"fmt"
#define F2P_ARG_USER_DISPLAY_NAME 		"userDisplayName"
#define F2P_ARG_USER_ICON 			"userIcon"
#define F2P_ARG_USER_NAME			"userName"
#define F2P_ARG_USER_ID				"userId"
#define F2P_ARG_COUNT 				"count"
#define F2P_ARG_HMAC_SALT 			"hmacSalt"
#define F2P_ARG_UP 				"up"
#define F2P_ARG_SIGNATURE_COUNT			"sigCount"
#define F2P_ARG_CLIENT_DATA_HASH		"clientdataHash"
#define F2P_ARG_HMAC_SECRET			"hmacSecret"

int f2p_register(f2p_creden_t *, sd_bus_message_handler_t, f2p_pin_req_callback_t);
int f2p_register_finish(sd_bus_message *, f2p_creden_t *);
int f2p_sign(f2p_assert_t *, sd_bus_message_handler_t);
int f2p_sign_finish(sd_bus_message *, f2p_assert_t *);
int f2p_cancel(void);

f2p_creden_t 	*f2p_creden_init();
f2p_assert_t 	*f2p_assert_init();
void		f2p_creden_free(f2p_creden_t **);
void		f2p_assert_free(f2p_assert_t **);

int f2p_creden_set_required(f2p_creden_t *, const char *, const uint8_t *, size_t, const char *, const uint8_t *, size_t, const char *);
int f2p_assert_set_required(f2p_assert_t *, const uint8_t *, size_t, const char *, const uint8_t *, size_t);
int f2p_creden_set_cose_type(f2p_creden_t *, const char *);
int f2p_creden_set_rk(f2p_creden_t *, uint8_t);
int f2p_creden_set_uv(f2p_creden_t *, uint8_t);
int f2p_creden_set_extension(f2p_creden_t *, uint8_t);
int f2p_creden_set_client_data_hash(f2p_creden_t *, const uint8_t *, size_t);
int f2p_creden_set_relying_party(f2p_creden_t *, const char *);
int f2p_creden_set_user_id(f2p_creden_t *, const uint8_t *, size_t);
int f2p_creden_set_user_name(f2p_creden_t *, const char *);
int f2p_creden_set_user_display_name(f2p_creden_t *, const char *);
int f2p_creden_set_user_icon(f2p_creden_t *, const char *);
int f2p_assert_set_up(f2p_assert_t *, uint8_t);
int f2p_assert_set_uv(f2p_assert_t *, uint8_t);
int f2p_assert_set_extension(f2p_assert_t *, uint8_t);
int f2p_assert_set_count(f2p_assert_t *, size_t);
int f2p_assert_set_client_data_hash(f2p_assert_t *, const uint8_t *, size_t);
int f2p_assert_set_allow_credential(f2p_assert_t *, const uint8_t *, size_t);
int f2p_assert_set_relying_party(f2p_assert_t *, const char *);

uint8_t 	*f2p_creden_get_authdata(f2p_creden_t *);
uint8_t 	*f2p_creden_get_signature(f2p_creden_t *);
uint8_t 	*f2p_creden_get_x5c(f2p_creden_t *);
uint8_t 	*f2p_creden_get_credential_id(f2p_creden_t *);
uint8_t 	*f2p_creden_get_public_key(f2p_creden_t *);
char 		*f2p_creden_get_fmt(f2p_creden_t *);
uint8_t 	*f2p_creden_get_client_data_hash(f2p_creden_t *);
size_t 		f2p_assert_get_count(f2p_assert_t *);
uint8_t 	*f2p_assert_get_authdata(f2p_assert_t *);
uint8_t 	*f2p_assert_get_client_data_hash(f2p_assert_t *);
uint8_t 	*f2p_assert_get_hmac_secret(f2p_assert_t *);
uint8_t 	*f2p_assert_get_user_id(f2p_assert_t *);
char 		*f2p_assert_get_user_display_name(f2p_assert_t *);
char 		*f2p_assert_get_user_icon(f2p_assert_t *);
char 		*f2p_assert_get_user_name(f2p_assert_t *);
uint8_t 	*f2p_assert_get_signature(f2p_assert_t *);
unsigned	f2p_assert_get_sig_count(f2p_assert_t *);

size_t 	f2p_creden_get_authdata_len(f2p_creden_t *);
size_t 	f2p_creden_get_signature_len(f2p_creden_t *);
size_t 	f2p_creden_get_x5c_len(f2p_creden_t *);
size_t 	f2p_creden_get_credential_id_len(f2p_creden_t *);
size_t 	f2p_creden_get_public_key_len(f2p_creden_t *);
size_t 	f2p_creden_get_client_data_hash_len(f2p_creden_t *);
size_t 	f2p_assert_get_authdata_len(f2p_assert_t *);
size_t 	f2p_assert_get_client_data_hash_len(f2p_assert_t *);
size_t 	f2p_assert_get_hmac_secret_len(f2p_assert_t *);
size_t 	f2p_assert_get_user_id_len(f2p_assert_t *);
size_t 	f2p_assert_get_signature_len(f2p_assert_t *);
