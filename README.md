# fido2-proxy

`fido2-proxy` provides a server and the client library that allows
communication between the host and the FIDO2 authenticator devices
over a D-Bus connection.

## Build

```sh
$ meson _build
$ ninja -C _build
```

## Running

```sh
_build/fido2-proxy-server
```

## Documentation

After a successful build, the documentation will be generated under
`_build/fido2-proxy-docs.html`.

## License

LGPLv3+ or GPLv2.
