/* SPDX-License-Identifier: LGPL-3.0-or-later OR GPL-2.0 */

/*
 * Copyright (C) 2020 Red Hat, Inc.
 *
 * This file is part of fido2-proxy.
 *
 * fido2-proxy is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * fido2-proxy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see https://www.gnu.org/licenses/.
 */

#include "config.h"

#include <systemd/sd-bus.h>
#include <systemd/sd-event.h>
#include <sys/mman.h>

#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <limits.h>

#include "log.h"
#include "server.h"

static pthread_t worker_thread;
static pthread_mutex_t worker_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t worker_cond = PTHREAD_COND_INITIALIZER;

static struct {
	const char *name;
	struct fp_backend *backend;
} backends[] = {
	{ "libfido2", &fp_libfido2_backend },
#if WITH_TPM2
	{ "tpm2", &fp_tpm2_backend },
#endif
};

static struct fp_backend *backend;

struct fp_request {
	sd_bus_slot *slot;
	char *object_path;
	void *closure;
	void (*closure_free)(void *);
	int (*dispatch)(sd_bus *, const char *, void *);
	struct fp_request *next;
};

static void
fp_request_free(struct fp_request *request)
{
	if (!request)
		return;

	free(request->object_path);
	sd_bus_slot_unref(request->slot);
	if (request->closure_free)
		request->closure_free(request->closure);
	free(request);
}

static inline void
fp_request_freep(void *p)
{
	struct fp_request *request = *(struct fp_request **)p;
	fp_request_free(request);
}

static int
handle_cancel(sd_bus_message *m, void *userdata, sd_bus_error *ret_error)
{
	struct fp_request *request = userdata;
	return backend->handle_cancel(m, request->closure);
}

static const sd_bus_vtable request_vtable[] = {
	SD_BUS_VTABLE_START(0),
	SD_BUS_METHOD("Cancel", NULL, NULL, handle_cancel, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_SIGNAL_WITH_NAMES("Error",
				 "us", SD_BUS_PARAM(code) SD_BUS_PARAM(cause),
				 0),
	SD_BUS_SIGNAL_WITH_NAMES("Completed",
				 "a{sv}", SD_BUS_PARAM(result),
				 0),
	SD_BUS_VTABLE_END
};

int
fp_server_enqueue_request(struct fp_server *server,
			  const char *request_object_path,
			  int (*dispatch)(sd_bus *, const char *, void *),
			  void *closure, void (*closure_free)(void *))
{
	_cleanup_(fp_request_freep) struct fp_request *request = NULL;
	int r;

	request = calloc(1, sizeof(struct fp_request));
	if (!request) {
		fp_log_errno(ENOMEM);
		return -ENOMEM;
	}

	request->object_path = strdup(request_object_path);
	request->closure = closure;
	request->closure_free = closure_free;
	request->dispatch = dispatch;

	r = sd_bus_add_object_vtable(server->bus,
				     &request->slot,
				     request->object_path,
				     FP_REQUEST_INTERFACE,
				     request_vtable, request);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	if (!server->requests.head)
		server->requests.head = server->requests.tail = request;
	else
		server->requests.tail->next = request;
	request = NULL;

	server->requests.n_requests++;

	pthread_mutex_lock(&worker_mutex);
	pthread_cond_signal(&worker_cond);
	pthread_mutex_unlock(&worker_mutex);

	return 0;
}

static struct fp_request *
fp_server_dequeue_request(struct fp_server *server)
{
	struct fp_request *request;

	if (!server->requests.head)
		return NULL;

	if (server->requests.tail == server->requests.head)
		server->requests.tail = NULL;

	request = server->requests.head;
	server->requests.head = server->requests.head->next;

	server->requests.n_requests--;

	return request;
}

void
fp_device_free(struct fp_device *device)
{
	if (!device)
		return;

	sd_bus_slot_unref(device->slot);
	free(device->path);
	free(device->object_path);
	free(device);
}

static int
fp_server_init(struct fp_server *server)
{
	int r;

	memset(server, 0, sizeof(struct fp_server));

	r = sd_event_default(&server->event);
	if (r < 0)
		return r;

	r = sd_bus_open_user(&server->bus);
	if (r < 0)
		return r;

	sd_bus_attach_event(server->bus, server->event,
			    SD_EVENT_PRIORITY_NORMAL);

	r = backend->server_startup(server);
	if (r < 0)
		return r;

	return 0;
}

static int
enumerate_devices(sd_bus *bus, const char *path, void *userdata, char ***nodes,
		  sd_bus_error *error)
{
	struct fp_server *server = userdata;
	struct fp_device *p;
	char **object_paths;
	size_t i;

	object_paths = calloc(server->n_devices + 1, sizeof(char *));
	if (!object_paths) {
		fp_log_errno(ENOMEM);
		return -ENOMEM;
	}

	for (i = 0, p = server->devices; p; p = p->next)
		object_paths[i] = strdup(p->object_path);

	*nodes = object_paths;
        return 1;
}

static void
fp_server_destroy(struct fp_server *server)
{
	struct fp_device *device;
	struct fp_request *request;

	sd_bus_detach_event(server->bus);
	sd_bus_close(server->bus);
	sd_bus_unref(server->bus);

	sd_event_unref(server->event);

	for (device = server->devices; device; ) {
		struct fp_device *next = device->next;
		fp_device_free(device);
		device = next;
	}

	for (request = server->requests.head; request; ) {
		struct fp_request *next = request->next;
		fp_request_free(request);
		request = next;
	}

	backend->server_teardown(server);
}

static int
fp_handle_server_sigterm(sd_event_source *s, const struct signalfd_siginfo *si, void *userdata) {
	struct fp_server *server = userdata;

	pthread_cond_signal(&worker_cond);
	pthread_kill(worker_thread, SIGTERM);
	sd_event_exit(server->event, 0);

	return 0;
}

static int
fp_server_startup(struct fp_server *server)
{
	int r;

	r = sd_bus_request_name(server->bus, FP_SERVICE_NAME, 0);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	backend->server_enumerate_devices(server);

	r = sd_bus_add_node_enumerator(server->bus, NULL, FP_DEVICE_PATH,
				       enumerate_devices, server);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_add_object_manager(server->bus, NULL, FP_DEVICE_PATH);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	sigset_t set;
	sigemptyset(&set);
	sigaddset(&set, SIGTERM);

	r = sigprocmask(SIG_BLOCK, &set, NULL);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_event_add_signal(server->event, NULL, SIGTERM, fp_handle_server_sigterm, server);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	return 0;
}

static int
fp_server_run(struct fp_server *server)
{
	return sd_event_loop(server->event);
}

static void *
worker(void *data)
{
	struct fp_server *server = data;

	pthread_mutex_lock(&worker_mutex);
	while (server->requests.n_requests == 0) {
		struct fp_request *request;
		int r;

		pthread_cond_wait(&worker_cond, &worker_mutex);

		request = fp_server_dequeue_request(server);
		if (!request)
			break;

		r = request->dispatch(server->bus, request->object_path, request->closure);
		if (r < 0)
			fp_log_errno(-r);

		fp_request_free(request);
	}
	pthread_mutex_unlock(&worker_mutex);

	return NULL;
}

static struct option long_options[] = {
	{ "backend", required_argument, 0, CHAR_MAX + 1 },
	{ "certfile", required_argument, 0, CHAR_MAX + 2 },
	{ "keyfile", required_argument, 0, CHAR_MAX + 3 },
	{ "debug", no_argument, 0, 'd' },
	{ "help", no_argument, 0, 'h' },
	{ 0, 0, 0, 0 },
};

static void
print_usage(FILE *out)
{
	fprintf(out,
		"Usage: fido2-proxy-server [OPTIONS]\n"
		"  where OPTIONS are:\n"
		"  --backend\tspecify backend (\"libfido2\" or \"tpm2\")\n"
		"  --certfile\tspecify X.509 certificate file for attestation\n"
		"  --keyfile\tspecify X.509 key file for attestation\n"
		"  -d, --debug\tenable debugging\n"
		"  -h, --help\tprint this message\n");
}

int
main(int argc, char **argv)
{
	struct fp_server server;
	int status = EXIT_SUCCESS;
	int r;
	bool opt_debug = false;
	const char *opt_backend = "libfido2";
	const char *opt_certfile = NULL;
	const char *opt_keyfile = NULL;
	size_t i;

	while (1) {
		int option_index = 0;
		int c;

		c = getopt_long(argc, argv, "d", long_options, &option_index);
		if (c == -1) {
			break;
		}
		switch (c) {
		case CHAR_MAX + 1: /* --backend */
			opt_backend = optarg;
			break;

		case CHAR_MAX + 2: /* --certfile */
			opt_certfile = optarg;
			break;

		case CHAR_MAX + 3: /* --keyfile */
			opt_keyfile = optarg;
			break;

		case 'd':
			opt_debug = true;
			break;

		case 'h':
			print_usage(stdout);
			return EXIT_SUCCESS;

		default:
			print_usage(stderr);
			return EXIT_FAILURE;
		}
	}

	if (optind < argc) {
		print_usage(stderr);
		return EXIT_FAILURE;
	}

	for (i = 0; i < sizeof(backends)/sizeof(*backends); i++) {
		if (streq(opt_backend, backends[i].name)) {
			backend = backends[i].backend;
		}
	}
	if (!backend) {
		fprintf(stderr, "Unknown backend: %s\n", opt_backend);
		print_usage(stderr);
		return EXIT_FAILURE;
	}

	r = backend->global_init(opt_debug, opt_certfile, opt_keyfile);
	if (r < 0) {
		fprintf(stderr, "Failed to initialize backend\n");
		return EXIT_FAILURE;
	}

	r = fp_server_init(&server);
	if (r < 0) {
		fprintf(stderr, "Failed to initialize server\n");
		return EXIT_FAILURE;
	}

	r = fp_server_startup(&server);
	if (r < 0) {
		fprintf(stderr, "Failed to startup server\n");
		status = EXIT_FAILURE;
		goto out;
	}

	r = pthread_create(&worker_thread, NULL, worker, &server);
	if (r < 0) {
		fprintf(stderr, "Failed to create worker thread\n");
		status = EXIT_FAILURE;
		goto out;
	}

	r = fp_server_run(&server);
	if (r < 0) {
		fprintf(stderr, "Failed to run server\n");
		status = EXIT_FAILURE;
		goto out;
	}

 out:
	fp_server_destroy(&server);
	if (worker_thread) {
		pthread_join(worker_thread, NULL);
	}

	return status;
}

void
lock_worker(void)
{
	pthread_mutex_lock(&worker_mutex);
}

void
unlock_worker(void)
{
	pthread_mutex_unlock(&worker_mutex);
}
