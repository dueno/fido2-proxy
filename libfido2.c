/* SPDX-License-Identifier: LGPL-3.0-or-later OR GPL-2.0 */

/*
 * Copyright (C) 2020 Red Hat, Inc.
 *
 * This file is part of fido2-proxy.
 *
 * fido2-proxy is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * fido2-proxy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see https://www.gnu.org/licenses/.
 */

#include "config.h"

#include <errno.h>
#include <fido.h>
#include <libudev.h>
#include <unistd.h>

#include "fido_check.h"
#include "log.h"
#include "server.h"

static inline void
udev_device_unrefp(void *p)
{
	struct udev_device *device = *(struct udev_device **)p;
	if (device)
		udev_device_unref(device);
}

static inline void
udev_enumerate_unrefp(void *p)
{
	struct udev_enumerate *enumerate = *(struct udev_enumerate **)p;
	if (enumerate)
		udev_enumerate_unref(enumerate);
}

struct base_closure {
	fido_dev_t *dev;
	bool cancelled;
};

struct make_cred_closure {
	fido_dev_t *dev;
	bool cancelled;
	fido_cred_t *cred;
	int pin_fd;
};

struct get_assert_closure {
	fido_dev_t *dev;
	bool cancelled;
	fido_assert_t *assert;
	int pin_fd;
	bool uv;
};

static int
request_make_cred(sd_bus *bus, const char *object_path, void *data)
{
	struct make_cred_closure *closure = data;
	_cleanup_(sd_bus_message_unrefp) sd_bus_message *m = NULL;
	int r;
	char buf[64];
	char *pin = NULL;

	memset(buf, 0, sizeof(64));
	if (closure->pin_fd != -1) {
		r = read(closure->pin_fd, buf, sizeof(buf) - 1);
		close(closure->pin_fd);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		pin = buf;
	}

	if (closure->cancelled) {
		r = new_error_signal(bus, &m, object_path,
				     FP_ERROR_CANCELLED,
				     "The request was cancelled");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		return sd_bus_send(bus, m, NULL);
	}

	r = fido_dev_make_cred(closure->dev, closure->cred, pin);

	if (r != FIDO_OK) {
		fp_error("failed to make credential: %s", fido_strerr(r));
		if (r == FIDO_ERR_PIN_REQUIRED) {
			r = new_error_signal(bus, &m, object_path,
					     FP_ERROR_PIN_REQUIRED,
					     "PIN is required");
		} else {
			r = new_error_signal(bus, &m, object_path,
					     FP_ERROR_PLATFORM_ERROR,
					     "Failed to make credential");
		}
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
	} else {
		r = sd_bus_message_new_signal(bus, &m, object_path,
					      FP_REQUEST_INTERFACE,
					      "Completed");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_ARRAY, "{sv}");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		r = append_bytearray_entry(m, "authData",
					   fido_cred_authdata_ptr(closure->cred),
					   fido_cred_authdata_len(closure->cred));
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		r = append_bytearray_entry(m, "signature",
					   fido_cred_sig_ptr(closure->cred),
					   fido_cred_sig_len(closure->cred));
		if (r < 0) {
			fp_log_errno(r);
			return r;
		}
		r = append_bytearray_entry(m, "credentialID",
					   fido_cred_id_ptr(closure->cred),
					   fido_cred_id_len(closure->cred));
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		r = append_bytearray_entry(m, "publicKey",
					   fido_cred_pubkey_ptr(closure->cred),
					   fido_cred_pubkey_len(closure->cred));
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		r = append_bytearray_entry(m, "x5c",
					   fido_cred_x5c_ptr(closure->cred),
					   fido_cred_x5c_len(closure->cred));
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		r = sd_bus_message_append(m, "{sv}", "fmt", "s", fido_cred_fmt(closure->cred));
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		r = append_bytearray_entry(m, "clientdataHash",
					   fido_cred_clientdata_hash_ptr(closure->cred),
					   fido_cred_clientdata_hash_len(closure->cred));
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		r = sd_bus_message_close_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
	}

	return sd_bus_send(bus, m, NULL);
}

static int
request_get_assertion(sd_bus *bus, const char *object_path, void *data)
{
	struct get_assert_closure *closure = data;
	_cleanup_(sd_bus_message_unrefp) sd_bus_message *m = NULL;
	int r;
	char buf[64];
	char *pin = NULL;

	memset(buf, 0, sizeof(buf));
	if (closure->pin_fd != -1) {
		if (closure->uv) {
			r = read(closure->pin_fd, buf, sizeof(buf) - 1);
			close(closure->pin_fd);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			pin = buf;
		} else {
			close(closure->pin_fd);
		}
	}

	if (closure->cancelled) {
		r = new_error_signal(bus, &m, object_path,
				     FP_ERROR_CANCELLED,
				     "The request was cancelled");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}

		return sd_bus_send(bus, m, NULL);
	}

	r = fido_dev_get_assert(closure->dev, closure->assert, pin);

	if (r != FIDO_OK) {
		fp_error("failed to get assertion: %s", fido_strerr(r));
		if (r == FIDO_ERR_PIN_REQUIRED) {
			r = new_error_signal(bus, &m, object_path,
					     FP_ERROR_PIN_REQUIRED,
					     "PIN is required");
		} else {
			r = new_error_signal(bus, &m, object_path,
					     FP_ERROR_PLATFORM_ERROR,
					     "Failed to get assertion");
		}
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
	} else {
		size_t idx_count = fido_assert_count(closure->assert);

		r = sd_bus_message_new_signal(bus, &m, object_path,
					      FP_REQUEST_INTERFACE,
					      "Completed");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_ARRAY, "{sv}");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}

		r = append_bytearray_entry(m, "clientdataHash",
					   fido_assert_clientdata_hash_ptr(closure->assert),
					   fido_assert_clientdata_hash_len(closure->assert));
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}

		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		r = sd_bus_message_append(m, "s", "assertions");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "a{sv}");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_ARRAY, "{sv}");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		for (size_t idx = 0; idx < idx_count; idx++) {
			r = sd_bus_message_append(m, "{sv}", "count", "t", idx);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = append_bytearray_entry(m, "authData",
						   fido_assert_authdata_ptr(closure->assert, idx),
						   fido_assert_authdata_len(closure->assert, idx));
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = append_bytearray_entry(m, "hmacSecret",
						   fido_assert_hmac_secret_ptr(closure->assert, idx),
						   fido_assert_hmac_secret_len(closure->assert, idx));
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = append_bytearray_entry(m, "userId",
						   fido_assert_user_id_ptr(closure->assert, idx),
						   fido_assert_user_id_len(closure->assert, idx));
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "{sv}", "userDisplayName",
						  "s", fido_assert_user_display_name(closure->assert, idx));
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "{sv}", "userIcon", "s",
						fido_assert_user_icon(closure->assert, idx));
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "{sv}", "userName", "s",
						  fido_assert_user_name(closure->assert, idx));
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = append_bytearray_entry(m, "signature",
						   fido_assert_sig_ptr(closure->assert, idx),
						   fido_assert_sig_len(closure->assert, idx));
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "{sv}", "sigCount", "u",
						  fido_assert_sigcount(closure->assert, idx));
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
		}
		r = sd_bus_message_close_container(m); /* array of {sv} */
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		r = sd_bus_message_close_container(m); /* variant of a{sv} */
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		r = sd_bus_message_close_container(m); /* dict entry of {"assertions", v} */
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		r = sd_bus_message_close_container(m); /* array of {sv} */
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
	}

	return sd_bus_send(bus, m, NULL);
}

static void
make_cred_closure_free(void *data)
{
	struct make_cred_closure *closure = data;

	if (!closure)
		return;

	fido_dev_free(&closure->dev);
	fido_cred_free(&closure->cred);
	free(closure);
}

static int
handle_make_credential(sd_bus_message *m, void *userdata,
		       sd_bus_error *ret_error)
{
	int r;
	const char *type;
	uint8_t *client_data_hash;
	size_t client_data_hash_len;
	const char *rp;
	uint8_t *user_id;
	size_t user_id_len;
	const char *user_name;
	const char *user_display_name = NULL;
	const char *user_icon = NULL;
	struct fp_device *device = userdata;
	int force_fido2 = false;
	int force_u2f = false;
	int pin_fd = -1;

	_cleanup_(fido_cred_free) fido_cred_t *cred = NULL;
	_cleanup_(fido_dev_free) fido_dev_t *dev = NULL;
	int cose_type;
	_cleanup_free_ char *request_object_path = NULL;
	struct make_cred_closure *closure;

	r = sd_bus_message_read_basic(m, 's', &type);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	if (streq(type, "es256"))
		cose_type = COSE_ES256;
	else if (streq(type, "rs256"))
		cose_type = COSE_RS256;
	else if (streq(type, "eddsa"))
		cose_type = COSE_EDDSA;
	else {
		fp_error("unknown type: \"%s\"", type);
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Unknown type: \"%s\"",
						  type);
	}

	r = sd_bus_message_read_array(m, 'y',
				      (const void **)&client_data_hash,
				      &client_data_hash_len);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_read_basic(m, 's', &rp);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_read_array(m, 'y', (const void **)&user_id, &user_id_len);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_read_basic(m, 's', &user_name);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	/* FIXME: check options */

	cred = fido_cred_new();
	if (!cred) {
		fp_error("failed to allocate fido_cred");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	/* REQUIRED ARGUMENTS */
	r = fido_cred_set_type(cred, cose_type);
	if (r != FIDO_OK) {
		fp_error("failed to set cred type: %s", fido_strerr(r));
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Failed to parse request");
	}

	r = fido_cred_set_clientdata_hash(cred, client_data_hash, client_data_hash_len);
	if (r != FIDO_OK) {
		fp_error("failed to set client data hash: %s", fido_strerr(r));
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Failed to parse request");
	}

	r = fido_cred_set_rp(cred, rp, NULL);
	if (r != FIDO_OK) {
		fp_error("failed to set rp: %s", fido_strerr(r));
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Failed to parse request");
	}
	/* REQUIRED ARGUMENTS END */

	/* OPTIONAL ARGUMENTS */
	r = sd_bus_message_enter_container(m, 'a', "{sv}");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	for (;;) {
		const char *label;

		r = sd_bus_message_enter_container(m, 'e', "sv");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		if (r == 0){
			break;
		}

		r = sd_bus_message_read_basic(m, 's', &label);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}

		if (streq(label, "extensions")) {
			uint8_t ext;

			r = sd_bus_message_read(m, "v", "y", &ext);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			switch (ext) {
				case FIDO_EXT_HMAC_SECRET:
					r = fido_cred_set_extensions(cred, FIDO_EXT_HMAC_SECRET);
					break;
				/*case FIDO_EXT_CRED_PROTECT:
					r = fido_cred_set_extensions(cred, FIDO_EXT_CRED_PROTECT);
					break;*/
				default:
					r = fido_cred_set_extensions(cred, 0);
					break;
			}
			if (r != FIDO_OK) {
				fp_error("failed to set extensions: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}

		} /*else if (streq(label, "protection")) {
			int prot;

			r = sd_bus_message_read(m, "v", "i", &prot);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = fido_cred_set_prot(cred, prot);
			if (r != FIDO_OK) {
				fp_error("failed to set protection: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}

		}*/ else if (streq(label, "rk")) {
			int rk;

			r = sd_bus_message_read(m, "v", "b", &rk);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = fido_cred_set_rk(cred, rk ? FIDO_OPT_TRUE : FIDO_OPT_FALSE);
			if (r != FIDO_OK) {
				fp_error("failed to set resident key: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}
		} else if (streq(label, "uv")) {
			int uv;

			r = sd_bus_message_read(m, "v", "b", &uv);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = fido_cred_set_uv(cred, uv ? FIDO_OPT_TRUE : FIDO_OPT_FALSE);
			if (r != FIDO_OK) {
				fp_error("failed to set user verification: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}
		} else if (streq(label, "fmt")) {
			char *fmt;

			r = sd_bus_message_read(m, "v", "s", &fmt);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = fido_cred_set_fmt(cred, fmt);
			if (r != FIDO_OK) {
				fp_error("failed to set protection: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}

		} else if (streq(label, "userDisplayName")) {
			r = sd_bus_message_read(m, "v", "s", &user_display_name);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

		} else if (streq(label, "userIcon")) {
			r = sd_bus_message_read(m, "v", "s", &user_icon);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

		} else if (streq(label, "forceFIDO2")) {
			r = sd_bus_message_read(m, "v", "b", &force_fido2);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

		} else if (streq(label, "forceU2F")) {
			r = sd_bus_message_read(m, "v", "b", &force_u2f);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

		} else if (streq(label, "pinFD")) {
			r = sd_bus_message_read(m, "v", "h", &pin_fd);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

		} else {
			r = sd_bus_message_skip(m, "v");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
		}

		r = sd_bus_message_exit_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
	}


	r = sd_bus_message_exit_container(m);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	if (force_fido2 && force_u2f) {
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Can't force FIDO2 and U2F at the same time");
	}

	/* OPTIONAL ARGUMENTS END */

	dev = fido_dev_new();
	if (!dev) {
		fp_error("failed to allocate fido_dev");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	r = fido_dev_open(dev, device->path);
	if (r != FIDO_OK) {
		fp_error("failed to open device: %s", fido_strerr(r));
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Failed to parse request");
	}

	if (force_fido2) {
		fido_dev_force_fido2(dev);
	} else if (force_u2f) {
		fido_dev_force_u2f(dev);
	}

	/* We call this only after all the optional arguments are parsed. */
	r = fido_cred_set_user(cred, user_id, user_id_len, user_name,
			       user_display_name, user_icon);
	if (r != FIDO_OK) {
		fp_error("failed to set user: %s", fido_strerr(r));
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Failed to parse request");
	}

	closure = calloc(1, sizeof(struct make_cred_closure));
	if (!closure) {
		fp_error("failed to allocate make_cred_closure");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	if (asprintf(&request_object_path, FP_REQUEST_PATH "/%lu",
		     device->server->requests.n_requests) < 0) {
		fp_error("failed to allocate request_object_path");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	closure->dev = dev;
	dev = NULL;
	closure->cred = cred;
	cred = NULL;
	closure->pin_fd = pin_fd;

	r = fp_server_enqueue_request(device->server, request_object_path,
				      request_make_cred, closure,
				      make_cred_closure_free);
	if (r < 0) {
		fp_error("failed to enqueue request");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	return sd_bus_reply_method_return(m, "o", request_object_path);
}

static void
get_assert_closure_free(void *data)
{
	struct get_assert_closure *closure = data;

	if (!closure)
		return;

	fido_dev_free(&closure->dev);
	fido_assert_free(&closure->assert);
	free(closure);
}

static int
handle_get_assertion(sd_bus_message *m, void *userdata, sd_bus_error *ret_error)
{
	int r;
	uint8_t *client_data_hash;
	size_t client_data_hash_len;
	uint8_t *allow_cred;
	size_t allow_cred_len;
	const char *rp;
	struct fp_device *device = userdata;
	int pin_fd = -1;

	_cleanup_(fido_assert_free) fido_assert_t *assert = NULL;
	_cleanup_(fido_dev_free) fido_dev_t *dev = NULL;
	_cleanup_free_ char *request_object_path = NULL;
	struct get_assert_closure *closure;

	closure = calloc(1, sizeof(struct get_assert_closure));
	if (!closure) {
		fp_error("failed to allocate get_assert_closure");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	closure->uv = false;

	r = sd_bus_message_read_array(m, 'y',
					(const void **)&client_data_hash,
					&client_data_hash_len);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_read_basic(m, 's', &rp);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_read_array(m, 'y',
					(const void **)&allow_cred,
					&allow_cred_len);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	assert = fido_assert_new();
	if (!assert) {
		fp_error("failed to allocate fido_assert");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	/* REQUIRED ARGUMENTS */
	r = fido_assert_set_clientdata_hash(assert, client_data_hash, client_data_hash_len);
	if (r != FIDO_OK) {
		fp_error("failed to set client data hash: %s", fido_strerr(r));
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Failed to parse request");
	}

	r = fido_assert_set_rp(assert, rp);
	if (r != FIDO_OK) {
		fp_error("failed to set rp: %s", fido_strerr(r));
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Failed to parse request");
	}

	r = fido_assert_allow_cred(assert, allow_cred, allow_cred_len);
	if (r != FIDO_OK) {
		fp_error("failed to allow credential: %s", fido_strerr(r));
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Failed to parse request");
	}
	/* REQUIRED ARGUMENTS END */

	/* OPTIONAL ARGUMENTS */
	r = sd_bus_message_enter_container(m, 'a', "{sv}");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	for (;;) {
		const char *label;

		r = sd_bus_message_enter_container(m, 'e', "sv");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		if (r == 0) {
			break;
		}

		r = sd_bus_message_read_basic(m, 's', &label);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}

		if (streq(label, "count")) {
			size_t n;

			r = sd_bus_message_read(m, "v", "t", &n);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = fido_assert_set_count(assert, n);
			if (r != FIDO_OK) {
				fp_error("failed to set count: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}

		} else if (streq(label, "extensions")) {
			uint8_t ext;

			r = sd_bus_message_read(m, "v", "y", &ext);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			switch (ext) {
				case FIDO_EXT_HMAC_SECRET:
					r = fido_assert_set_extensions(assert, FIDO_EXT_HMAC_SECRET);
					break;
				/*case FIDO_EXT_CRED_PROTECT:
					r = fido_assert_set_extensions(assert, FIDO_EXT_CRED_PROTECT);
					break;*/
				default:
					r = fido_assert_set_extensions(assert, 0);
					break;
			}
			if (r != FIDO_OK) {
				fp_error("failed to set extensions: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}

		} else if (streq(label, "hmacSalt")) {
			uint8_t *hmac_ptr;
			size_t hmac_len;

			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = sd_bus_message_read_array(m, 'y',
							(const void **)&hmac_ptr,
							&hmac_len);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = fido_assert_set_hmac_salt(assert, hmac_ptr, hmac_len);
			if (r != FIDO_OK) {
				fp_error("failed to set hmac salt: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}

			r = sd_bus_message_exit_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

		} else if (streq(label, "up")) {
			int up;

			r = sd_bus_message_read(m, "v", "b", &up);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = fido_assert_set_up(assert, up ? FIDO_OPT_TRUE : FIDO_OPT_FALSE);
			if (r != FIDO_OK) {
				fp_error("failed to set up: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}
		} else if (streq(label, "uv")) {
			int uv;

			r = sd_bus_message_read(m, "v", "b", &uv);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = fido_assert_set_uv(assert, uv ? FIDO_OPT_TRUE : FIDO_OPT_FALSE);
			if (r != FIDO_OK) {
				fp_error("failed to set uv: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "failed to parse request");
			}
		} else if (streq(label, "pinFD")) {
			r = sd_bus_message_read(m, "v", "h", &pin_fd);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

		} else {
			r = sd_bus_message_skip(m, "v");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
		}

		r = sd_bus_message_exit_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
	}

	r = sd_bus_message_exit_container(m);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	/* OPTIONAL ARGUMENTS END */

	dev = fido_dev_new();
	if (!dev) {
		fp_error("failed to allocate fido_dev");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	r = fido_dev_open(dev, device->path);
	if (r != FIDO_OK) {
		fp_error("failed to open device: %s", fido_strerr(r));
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Failed to parse request");
	}

	if (asprintf(&request_object_path, FP_REQUEST_PATH "/%lu",
		     device->server->requests.n_requests) < 0) {
		fp_error("failed to allocate request_object_path");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	closure->dev = dev;
	dev = NULL;
	closure->assert = assert;
	assert = NULL;
	closure->pin_fd = pin_fd;

	r = fp_server_enqueue_request(device->server, request_object_path,
				      request_get_assertion, closure,
				      get_assert_closure_free);
	if (r < 0) {
		fp_error("failed to enqueue request");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	return sd_bus_reply_method_return(m, "o", request_object_path);
}

static const sd_bus_vtable device_vtable[] = {
	SD_BUS_VTABLE_START(0),
	SD_BUS_PROPERTY("Path", "s", NULL, offsetof(struct fp_device, path),
			SD_BUS_VTABLE_PROPERTY_CONST),
	SD_BUS_METHOD_WITH_NAMES("MakeCredential", "saysaysa{sv}", SD_BUS_PARAM(type) SD_BUS_PARAM(client_data_hash) SD_BUS_PARAM(rp) SD_BUS_PARAM(user_id) SD_BUS_PARAM(user_name) SD_BUS_PARAM(options), "o", SD_BUS_PARAM(handle),
				 handle_make_credential, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_METHOD_WITH_NAMES("GetAssertion",
				 "aysaya{sv}", SD_BUS_PARAM(client_data_hash) SD_BUS_PARAM(rp) SD_BUS_PARAM(allow_cred) SD_BUS_PARAM(options),
				 "o", SD_BUS_PARAM(handle),
				 handle_get_assertion, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_VTABLE_END
};

static void
fp_server_move_device(struct fp_server *server,
		      struct fp_device *device,
		      struct udev_device *d)
{
	const char *path;

	if (!device)
		return;

	path = udev_device_get_devnode(d);
	if (!path)
		return;

	free(device->path);
	device->path = strdup(path);
}

static int
add_device(struct fp_server *server, struct udev_device *d, bool emit_signal)
{
	_cleanup_(fp_device_freep) struct fp_device *device = NULL;
	const char *path;
	int r;

	path = udev_device_get_devnode(d);
	if (!path)
		return -EINVAL;

	if (!is_fido(path))
		return 0;

	device = calloc(1, sizeof(struct fp_device));
	if (!device)
		return -ENOMEM;

	device->server = server;

	if (asprintf(&device->object_path, FP_DEVICE_PATH "/%lu",
		     server->n_devices) < 0)
		return -ENOMEM;

	device->path = strdup(path);
	if (!device->path)
		return -ENOMEM;

	r = sd_bus_add_object_vtable(server->bus, &device->slot,
				     device->object_path,
				     FP_DEVICE_INTERFACE,
				     device_vtable, device);
	if (r < 0)
		return r;

	if (emit_signal) {
		r = sd_bus_emit_object_added(server->bus, device->object_path);
		if (r < 0)
			return r;
	}

	device->next = server->devices;
	if (server->devices)
		server->devices->prev = device;
	server->devices = device;
	device = NULL;

	server->n_devices++;

	return 0;
}

static int
fp_server_add_device(struct fp_server *server, struct udev_device *d)
{
	return add_device(server, d, true);
}

static struct fp_device *
fp_server_find_device(struct fp_server *server,
		      struct udev_device *d)
{
	struct fp_device *p;
	const char *path;

	if (!d)
		return NULL;

	path = udev_device_get_devnode(d);
	if (!path)
		return NULL;

	for (p = server->devices; p; p = p->next)
		if (strcmp(p->path, path) == 0)
			return p;

	return NULL;
}

static void
fp_server_remove_device(struct fp_server *server, struct fp_device *device)
{
	if (!device)
		return;

	if (server->devices == device) {
		server->devices = device->next;
		if (device->next)
			device->next->prev = NULL;
	} else {
		if (device->next)
			device->next->prev = device->prev;
		if (device->prev)
			device->prev->next = device->next;
	}

	server->n_devices--;

	sd_bus_slot_unrefp(&device->slot);
	sd_bus_emit_object_removed(server->bus, device->object_path);

	fp_device_free(device);
}

static int
fp_udev_monitor(sd_event_source *source,
		int fd,
		uint32_t mask,
		void *data)
{
	struct fp_server *server = data;
	struct fp_device *device;
	_cleanup_(udev_device_unrefp) struct udev_device *d = NULL;
	const char *action;

	d = udev_monitor_receive_device(server->udev_monitor);
	if (!d)
		return 0;

	device = fp_server_find_device(server, d);
	action = udev_device_get_action(d);

	lock_worker();

	if (action && streq(action, "remove"))
		fp_server_remove_device(server, device);
	else if (action && streq(action, "move"))
		fp_server_move_device(server, device, d);
	else
		fp_server_add_device(server, d);

	unlock_worker();

	return 0;
}

static int
server_startup(struct fp_server *server)
{
	int r;

	server->udev = udev_new();
	if (!server->udev)
		return -ENOMEM;

	server->udev_monitor = udev_monitor_new_from_netlink(server->udev, "udev");
	if (!server->udev_monitor)
		return -ENOMEM;

	r = udev_monitor_filter_add_match_subsystem_devtype(server->udev_monitor,
							    "hidraw", NULL);
	if (r < 0)
		return r;

	r = udev_monitor_enable_receiving(server->udev_monitor);
	if (r < 0)
		return r;

	r = sd_event_add_io(server->event,
			    &server->udev_monitor_source,
			    udev_monitor_get_fd(server->udev_monitor),
			    EPOLLHUP | EPOLLERR | EPOLLIN,
			    fp_udev_monitor,
			    server);
	if (r < 0)
		return r;

	return 0;
}

static void
server_teardown(struct fp_server *server)
{
	udev_monitor_unref(server->udev_monitor);
	udev_unref(server->udev);
}

static int
server_enumerate_devices(struct fp_server *server)
{
	_cleanup_(udev_enumerate_unrefp) struct udev_enumerate *enumerate = NULL;
	struct udev_list_entry *devices, *dev_list_entry;
	int r;

	enumerate = udev_enumerate_new(server->udev);
	if (!enumerate)
		return -ENOMEM;

	udev_enumerate_add_match_subsystem(enumerate, "hidraw");
	r = udev_enumerate_scan_devices(enumerate);
	if (r < 0)
		return r;

	devices = udev_enumerate_get_list_entry(enumerate);
	if (!devices)
		return 0;

	udev_list_entry_foreach(dev_list_entry, devices){
		struct udev_device *d;
		const char *path;

		path = udev_list_entry_get_name(dev_list_entry);
		d = udev_device_new_from_syspath(server->udev, path);

		r = add_device(server, d, 0);
		if (r < 0)
			fp_error("failed to add device: %s", path);
		udev_device_unref(d);
	}

	return 0;
}

static int
handle_cancel(sd_bus_message *m, void *_closure)
{
	struct base_closure *closure = _closure;

	if (closure->cancelled) {
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Operation already cancelled");
	}
	closure->cancelled = true;

	fido_dev_cancel(closure->dev);

	return sd_bus_reply_method_return(m, NULL, NULL);
}

static int
global_init(bool debug, const char *certfile, const char *keyfile)
{
	if (debug) {
		fido_init(FIDO_DEBUG);
	}
	return 0;
}

struct fp_backend fp_libfido2_backend = {
	.server_startup = server_startup,
	.server_teardown = server_teardown,
	.server_enumerate_devices = server_enumerate_devices,
	.handle_cancel = handle_cancel,
	.global_init = global_init,
};
