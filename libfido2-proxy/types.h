/* SPDX-License-Identifier: LGPL-3.0-or-later OR GPL-2.0 */

/*
 * Copyright (C) 2020 Red Hat, Inc.
 *
 * This file is part of fido2-proxy.
 *
 * fido2-proxy is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * fido2-proxy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see https://www.gnu.org/licenses/.
 */

#ifndef __FIDO2_PROXY_TYPES_H__
#define __FIDO2_PROXY_TYPES_H__

#include <stdint.h>
#include <stdlib.h>
#include <systemd/sd-bus.h>

#define F2P_ARG_NOT_SET		255

typedef int (*f2p_pin_req_callback_t)(int);

#ifdef _FIDO2_PROXY_INTERNAL_
typedef struct creden {
	uint8_t		rk;
	uint8_t		uv;
	uint8_t		extensions;
	const char 	*cose_type;
	const char 	*relying_party;
	const char 	*user_name;
	const char 	*user_display_name;
	const char	*user_icon;
	uint8_t 	*client_data_hash;
	uint8_t 	*user_id;
	uint8_t 	*authdata;
	uint8_t		*x5c;
	uint8_t		*signature;
	uint8_t		*credential_id;
	uint8_t		*public_key;
	char		*fmt;
	size_t		user_id_len;
	size_t		authdata_len;
	size_t		x5c_len;
	size_t		signature_len;
	size_t		public_key_len;
	size_t		credential_id_len;
	size_t		client_data_hash_len;
} f2p_creden_t;

typedef struct assert {
	uint8_t		up;
	uint8_t		uv;
	uint8_t		extensions;
	uint8_t		*client_data_hash;
	const char	*relying_party;
	uint8_t		*allow_credential;
	uint8_t		*authdata;
	uint8_t		*hmac_secret;
	uint8_t		*signature;
	uint8_t		*user_id;
	char		*user_display_name;
	char		*user_icon;
	char		*user_name;
	unsigned	sig_count;
	size_t		count;
	size_t		client_data_hash_len;
	size_t		allow_credential_len;
	size_t		authdata_len;
	size_t		hmac_secret_len;
	size_t		signature_len;
	size_t		user_id_len;
} f2p_assert_t;

#else
typedef struct creden f2p_creden_t;
typedef struct assert f2p_assert_t;
#endif /* _FIDO2_PROXY_INTERNAL */

#endif /* __FIDO2_PROXY_TYPES_H__ */
