/* SPDX-License-Identifier: LGPL-3.0-or-later OR GPL-2.0 */

/*
 * Copyright (C) 2020 Red Hat, Inc.
 *
 * This file is part of fido2-proxy.
 *
 * fido2-proxy is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * fido2-proxy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see https://www.gnu.org/licenses/.
 */

#include "types.h"
#include "err.h"

#include <string.h>
#include <stdint.h>
#include <fido.h>

static inline int
set_array(uint8_t **dest, size_t *dest_len, const uint8_t *src, size_t src_len) {
	if (*dest)
		free(*dest);

	if (!src || !src_len) {
		*dest = NULL;
		*dest_len = 0;

		return F2P_ERR_INVALID_ARGUMENT;
	}

	*dest = calloc(src_len, 1);
	if (!*dest)
		return F2P_ERR_INTERNAL;

	if (src && !memcpy(*dest, src, src_len)) {
		free(*dest);
		*dest = NULL;

		return F2P_ERR_INTERNAL;
	}
	*dest_len = src_len;

	return F2P_OK;
}

/* -------------------------------------------------------------------------------------------------- */

f2p_creden_t *
f2p_creden_init() {
	f2p_creden_t *c = calloc(1, sizeof(f2p_creden_t));
	if (!c)
		return NULL;

	c->rk = F2P_ARG_NOT_SET;
	c->uv = F2P_ARG_NOT_SET;
	c->extensions = F2P_ARG_NOT_SET;
	c->cose_type = NULL;
	c->client_data_hash = NULL;
	c->relying_party = NULL;
	c->user_id = NULL;
	c->user_name = NULL;
	c->authdata = NULL;
	c->x5c = NULL;
	c->signature = NULL;
	c->fmt = NULL;
	c->user_display_name = NULL;
	c->user_icon = NULL;
	c->credential_id = NULL;
	c->public_key = NULL;

	return c;
}

void
f2p_creden_free(f2p_creden_t **c) {
	if ((*c)->client_data_hash)
		free((*c)->client_data_hash);
	if ((*c)->user_id)
		free((*c)->user_id);
	if ((*c)->authdata)
		free((*c)->authdata);
	if ((*c)->x5c)
		free((*c)->x5c);
	if ((*c)->signature)
		free((*c)->signature);
	if ((*c)->fmt)
		free((*c)->fmt);
	if ((*c)->credential_id)
		free((*c)->credential_id);
	if ((*c)->public_key)
		free((*c)->public_key);

	free(*c);
}

int
f2p_creden_set_cose_type(f2p_creden_t *cred, const char *cose_type) {
	if (!cred)
		return F2P_ERR_INVALID_ARGUMENT;

	cred->cose_type = cose_type;

	return F2P_OK;
}

int
f2p_creden_set_rk(f2p_creden_t *cred, uint8_t rk) {	
	cred->rk = rk ? FIDO_OPT_TRUE : FIDO_OPT_FALSE;

	return F2P_OK;
}

int
f2p_creden_set_uv(f2p_creden_t *cred, uint8_t uv) {
	cred->uv = uv ? FIDO_OPT_TRUE : FIDO_OPT_FALSE;

	return F2P_OK;
}

int
f2p_creden_set_extension(f2p_creden_t *cred, uint8_t ext) {
	if (!cred)
		return F2P_ERR_INVALID_ARGUMENT;

	if (ext == 0) {
		cred->extensions = 0;
	} else if (ext == FIDO_EXT_HMAC_SECRET) { //||
		   //ext == FIDO_EXT_CRED_PROTECT) {
		cred->extensions |= ext;
	} else {
		return F2P_ERR_INVALID_ARGUMENT;
	}

	return F2P_OK;
}

int
f2p_creden_set_client_data_hash(f2p_creden_t *cred, const uint8_t *client_data_hash, size_t client_data_hash_len) {
	if (!cred)
		return F2P_ERR_INVALID_ARGUMENT;

	return set_array(&(cred->client_data_hash), &(cred->client_data_hash_len), \
			client_data_hash, client_data_hash_len);
}

int
f2p_creden_set_relying_party(f2p_creden_t *cred, const char *relying_party) {
	if (!cred)
		return F2P_ERR_INVALID_ARGUMENT;

	cred->relying_party = relying_party;

	return F2P_OK;
}

int
f2p_creden_set_user_id(f2p_creden_t *cred, const uint8_t *user_id, size_t user_id_len) {
	if (!cred)
		return F2P_ERR_INVALID_ARGUMENT;

	return set_array(&(cred->user_id), &(cred->user_id_len), \
			user_id, user_id_len);
}

int
f2p_creden_set_user_name(f2p_creden_t *cred, const char *user_name) {
	if (!cred)
		return F2P_ERR_INVALID_ARGUMENT;

	cred->user_name = user_name;

	return F2P_OK;
}

static int
f2p_creden_set_authdata(f2p_creden_t *cred, const uint8_t *authdata, size_t authdata_len) {
	if (!cred)
		return F2P_ERR_INVALID_ARGUMENT;

	return set_array(&(cred->authdata), &(cred->authdata_len), \
			authdata, authdata_len);
}

static int
f2p_creden_set_x5c(f2p_creden_t *cred, const uint8_t *x5c, size_t x5c_len) {
	if (!cred)
		return F2P_ERR_INVALID_ARGUMENT;

	return set_array(&(cred->x5c), &(cred->x5c_len), \
			x5c, x5c_len);
}

static int
f2p_creden_set_signature(f2p_creden_t *cred, const uint8_t *signature, size_t signature_len) {
	if (!cred)
		return F2P_ERR_INVALID_ARGUMENT;

	return set_array(&(cred->signature), &(cred->signature_len), \
			signature, signature_len);
}

static int
f2p_creden_set_credential_id(f2p_creden_t *cred, const uint8_t *credential_id, size_t credential_id_len) {
	if (!cred)
		return F2P_ERR_INVALID_ARGUMENT;

	return set_array(&(cred->credential_id), &(cred->credential_id_len), \
			credential_id, credential_id_len);
}

static int
f2p_creden_set_public_key(f2p_creden_t *cred, const uint8_t *public_key, size_t public_key_len) {
	if (!cred)
		return F2P_ERR_INVALID_ARGUMENT;

	return set_array(&(cred->public_key), &(cred->public_key_len), \
			public_key, public_key_len);
}

static int
f2p_creden_set_fmt(f2p_creden_t *cred, const char *fmt) {
	if (!cred || !fmt)
		return F2P_ERR_INVALID_ARGUMENT;

	if (cred->fmt)
		free(cred->fmt);

	cred->fmt = calloc(strlen(fmt) + 1, 1);
	if (!cred->fmt)
		return F2P_ERR_INTERNAL;

	if (fmt && !strcpy(cred->fmt, fmt)) {
		free(cred->fmt);
		cred->fmt = NULL;

		return F2P_ERR_INTERNAL;
	}

	return F2P_OK;
}

int
f2p_creden_set_user_display_name(f2p_creden_t *cred, const char *user_display_name) {
	if (!cred)
		return F2P_ERR_INVALID_ARGUMENT;

	cred->user_display_name = user_display_name;

	return F2P_OK;
}

int
f2p_creden_set_user_icon(f2p_creden_t *cred, const char *user_icon) {
	if (!cred)
		return F2P_ERR_INVALID_ARGUMENT;

	cred->user_icon = user_icon;

	return F2P_OK;
}

int
f2p_creden_set_required(f2p_creden_t *cred, const char *cose_type, const uint8_t *client_data_hash, 
		    size_t client_data_hash_len, const char *relying_party, const uint8_t *user_id,
		    size_t user_id_len, const char *user_name) {
	int r;

	r = f2p_creden_set_cose_type(cred, cose_type);
	if (r < 0)
		return r;

	r = f2p_creden_set_client_data_hash(cred, client_data_hash, client_data_hash_len);
	if (r < 0)
		return r;

	r = f2p_creden_set_relying_party(cred, relying_party);
	if (r < 0)
		return r;

	r = f2p_creden_set_user_id(cred, user_id, user_id_len);
	if (r < 0)
		return r;

	r = f2p_creden_set_user_name(cred, user_name);

	return r;
}

/* -------------------------------------------------------------------------------------------------- */

f2p_assert_t *
f2p_assert_init() {
	f2p_assert_t *a = calloc(1, sizeof(f2p_assert_t));
	if (!a)
		return NULL;

	a->up = F2P_ARG_NOT_SET;
	a->uv = F2P_ARG_NOT_SET;
	a->extensions = F2P_ARG_NOT_SET;
	a->client_data_hash = NULL;
	a->relying_party = NULL;
	a->allow_credential = NULL;
	a->authdata = NULL;
	a->hmac_secret = NULL;
	a->signature = NULL;
	a->user_id = NULL;
	a->user_display_name = NULL;
	a->user_icon = NULL;
	a->user_name = NULL;

	return a;
}

void
f2p_assert_free(f2p_assert_t **a) {
	if ((*a)->client_data_hash)
		free((*a)->client_data_hash);
	if ((*a)->allow_credential)
		free((*a)->allow_credential);
	if ((*a)->authdata)
		free((*a)->authdata);
	if ((*a)->hmac_secret)
		free((*a)->hmac_secret);
	if ((*a)->signature)
		free((*a)->signature);
	if ((*a)->user_id)
		free((*a)->user_id);

	free(*a);
}

int
f2p_assert_set_up(f2p_assert_t *assert, uint8_t up) {
	assert->up = up ? FIDO_OPT_TRUE : FIDO_OPT_FALSE;

	return F2P_OK;
}

int
f2p_assert_set_uv(f2p_assert_t *assert, uint8_t uv) {
	assert->uv = uv ? FIDO_OPT_TRUE : FIDO_OPT_FALSE;

	return F2P_OK;
}

int
f2p_assert_set_extension(f2p_assert_t *assert, uint8_t ext) {
	if (!assert)
		return F2P_ERR_INVALID_ARGUMENT;

	if (ext == 0) {
		assert->extensions = 0;
	} else if (ext == FIDO_EXT_HMAC_SECRET) { //||
		   //ext == FIDO_EXT_CRED_PROTECT) {
		assert->extensions |= ext;
	} else {
		return F2P_ERR_INVALID_ARGUMENT;
	}

	return F2P_OK;
}

static int
f2p_assert_set_sig_count(f2p_assert_t *assert, unsigned sig_count) {
	assert->sig_count = sig_count;

	return F2P_OK;
}

int
f2p_assert_set_count(f2p_assert_t *assert, size_t count) {
	assert->count = count;

	return F2P_OK;
}

int
f2p_assert_set_client_data_hash(f2p_assert_t *assert, const uint8_t *client_data_hash, size_t client_data_hash_len) {
	if (!assert)
		return F2P_ERR_INVALID_ARGUMENT;

	return set_array(&(assert->client_data_hash), &(assert->client_data_hash_len), \
			client_data_hash, client_data_hash_len);
}

int
f2p_assert_set_allow_credential(f2p_assert_t *assert, const uint8_t *allow_credential, size_t allow_credential_len) {
	if (!assert)
		return F2P_ERR_INVALID_ARGUMENT;

	return set_array(&(assert->allow_credential), &(assert->allow_credential_len), \
			allow_credential, allow_credential_len);
}

static int
f2p_assert_set_authdata(f2p_assert_t *assert, const uint8_t *authdata, size_t authdata_len) {
	if (!assert)
		return F2P_ERR_INVALID_ARGUMENT;

	return set_array(&(assert->authdata), &(assert->authdata_len), \
			authdata, authdata_len);
}

static int
f2p_assert_set_hmac_secret(f2p_assert_t *assert, const uint8_t *hmac_secret, size_t hmac_secret_len) {
	if (!assert)
		return F2P_ERR_INVALID_ARGUMENT;

	return set_array(&(assert->hmac_secret), &(assert->hmac_secret_len), \
			hmac_secret, hmac_secret_len);
}

static int
f2p_assert_set_signature(f2p_assert_t *assert, const uint8_t *signature, size_t signature_len) {
	if (!assert)
		return F2P_ERR_INVALID_ARGUMENT;

	return set_array(&(assert->signature), &(assert->signature_len), \
			signature, signature_len);
}

static int
f2p_assert_set_user_id(f2p_assert_t *assert, const uint8_t *user_id, size_t user_id_len) {
	if (!assert)
		return F2P_ERR_INVALID_ARGUMENT;

	return set_array(&(assert->user_id), &(assert->user_id_len), \
			user_id, user_id_len);
}

int
f2p_assert_set_relying_party(f2p_assert_t *assert, const char *relying_party) {
	if (!assert)
		return F2P_ERR_INVALID_ARGUMENT;

	assert->relying_party = relying_party;

	return F2P_OK;
}

static int
f2p_assert_set_user_display_name(f2p_assert_t *assert, const char *user_display_name) {
	if (!assert || !user_display_name)
		return F2P_ERR_INVALID_ARGUMENT;

	if (assert->user_display_name)
		free(assert->user_display_name);

	assert->user_display_name = calloc(strlen(user_display_name) + 1, 1);
	if (!assert->user_display_name)
		return F2P_ERR_INTERNAL;

	if (user_display_name && !strcpy(assert->user_display_name, user_display_name)) {
		free(assert->user_display_name);
		assert->user_display_name = NULL;

		return F2P_ERR_INTERNAL;
	}

	return F2P_OK;
}

static int
f2p_assert_set_user_icon(f2p_assert_t *assert, const char *user_icon) {
	if (!assert || !user_icon)
		return F2P_ERR_INVALID_ARGUMENT;

	if (assert->user_icon)
		free(assert->user_icon);

	assert->user_icon = calloc(strlen(user_icon) + 1, 1);
	if (!assert->user_icon)
		return F2P_ERR_INTERNAL;

	if (user_icon && !strcpy(assert->user_icon, user_icon)) {
		free(assert->user_icon);
		assert->user_icon = NULL;

		return F2P_ERR_INTERNAL;
	}

	return F2P_OK;
}

static int
f2p_assert_set_user_name(f2p_assert_t *assert, const char *user_name) {
	if (!assert || !user_name)
		return F2P_ERR_INVALID_ARGUMENT;

	if (assert->user_name)
		free(assert->user_name);

	assert->user_name = calloc(strlen(user_name) + 1, 1);
	if (!assert->user_name)
		return F2P_ERR_INTERNAL;

	if (user_name && !strcpy(assert->user_name, user_name)) {
		free(assert->user_name);
		assert->user_name = NULL;

		return F2P_ERR_INTERNAL;
	}

	return F2P_OK;
}

int
f2p_assert_set_required(f2p_assert_t *assert, const uint8_t *client_data_hash, size_t client_data_hash_len,
	       	const char *relying_party, const uint8_t *allow_credential, size_t allow_credential_len) {
	int r;

	r = f2p_assert_set_client_data_hash(assert, client_data_hash, client_data_hash_len);
	if (r < 0)
		return r;

	r = f2p_assert_set_relying_party(assert, relying_party);
	if (r < 0)
		return r;

	r = f2p_assert_set_allow_credential(assert, allow_credential, allow_credential_len);

	return r;
}

/* -------------------------------------------------------------------------------------------------- */

uint8_t *
f2p_creden_get_authdata(f2p_creden_t *cred) {
	return cred->authdata;
}

uint8_t *
f2p_creden_get_signature(f2p_creden_t *cred) {
	return cred->signature;
}

uint8_t *
f2p_creden_get_x5c(f2p_creden_t *cred) {
	return cred->x5c;
}

uint8_t *
f2p_creden_get_credential_id(f2p_creden_t *cred) {
	return cred->credential_id;
}

uint8_t *
f2p_creden_get_public_key(f2p_creden_t *cred) {
	return cred->public_key;
}

char *
f2p_creden_get_fmt(f2p_creden_t *cred) {
	return cred->fmt;
}

uint8_t *
f2p_creden_get_client_data_hash(f2p_creden_t *cred) {
	return cred->client_data_hash;
}

/* -------------------------------------------------------------------------------------------------- */

size_t
f2p_assert_get_count(f2p_assert_t *assert) {
	return assert->count;
}

uint8_t *
f2p_assert_get_authdata(f2p_assert_t *assert) {
	return assert->authdata;
}

uint8_t *
f2p_assert_get_client_data_hash(f2p_assert_t *assert) {
	return assert->client_data_hash;
}

uint8_t *
f2p_assert_get_hmac_secret(f2p_assert_t *assert) {
	return assert->hmac_secret;
}

uint8_t *
f2p_assert_get_user_id(f2p_assert_t *assert) {
	return assert->user_id;
}

char *
f2p_assert_get_user_display_name(f2p_assert_t *assert) {
	return assert->user_display_name;
}

char *
f2p_assert_get_user_icon(f2p_assert_t *assert) {
	return assert->user_icon;
}

char *
f2p_assert_get_user_name(f2p_assert_t *assert) {
	return assert->user_name;
}

uint8_t *
f2p_assert_get_signature(f2p_assert_t *assert) {
	return assert->signature;
}

unsigned
f2p_assert_get_sig_count(f2p_assert_t *assert) {
	return assert->sig_count;
}

/* -------------------------------------------------------------------------------------------------- */

size_t 	f2p_creden_get_authdata_len(f2p_creden_t *cred) {
	return cred->authdata_len;
}

size_t 	f2p_creden_get_signature_len(f2p_creden_t *cred) {
	return cred->signature_len;
}

size_t 	f2p_creden_get_x5c_len(f2p_creden_t *cred) {
	return cred->x5c_len;
}

size_t 	f2p_creden_get_credential_id_len(f2p_creden_t *cred) {
	return cred->credential_id_len;
}

size_t 	f2p_creden_get_public_key_len(f2p_creden_t *cred) {
	return cred->public_key_len;
}

size_t 	f2p_creden_get_client_data_hash_len(f2p_creden_t *cred) {
	return cred->client_data_hash_len;
}

size_t 	f2p_assert_get_authdata_len(f2p_assert_t *assert) {
	return assert->authdata_len;
}

size_t 	f2p_assert_get_client_data_hash_len(f2p_assert_t *assert) {
	return assert->client_data_hash_len;
}

size_t 	f2p_assert_get_hmac_secret_len(f2p_assert_t *assert) {
	return assert->hmac_secret_len;
}

size_t 	f2p_assert_get_user_id_len(f2p_assert_t *assert) {
	return assert->user_id_len;
}

size_t 	f2p_assert_get_signature_len(f2p_assert_t *assert) {
	return assert->signature_len;
}
