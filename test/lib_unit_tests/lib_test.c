/* SPDX-License-Identifier: LGPL-3.0-or-later OR GPL-2.0 */

/*
 * Copyright (C) 2020 Red Hat, Inc.
 *
 * This file is part of fido2-proxy.
 *
 * fido2-proxy is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * fido2-proxy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see https://www.gnu.org/licenses/.
 */

#include "fido2-proxy.h"
#include "util.c"
#include "authenticator.c"
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <fido.h>
#include <fcntl.h>
#include <unistd.h>

void test_cred_set_cose_type(void **state) {
	f2p_creden_t *c = *state;
	int r;

	r = f2p_creden_set_cose_type(NULL, "baad");
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_creden_set_cose_type(NULL, NULL);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_creden_set_cose_type(c, ""), F2P_OK);
	assert_string_equal(c->cose_type, "");
	assert_return_code(f2p_creden_set_cose_type(c, "this is a string"), F2P_OK);
	assert_string_equal(c->cose_type, "this is a string");
	assert_return_code(f2p_creden_set_cose_type(c, "es256"), F2P_OK);
	assert_string_equal(c->cose_type, "es256");
	assert_return_code(f2p_creden_set_cose_type(c, "rs256"), F2P_OK);
	assert_string_equal(c->cose_type, "rs256");
	assert_return_code(f2p_creden_set_cose_type(c, "eddsa"), F2P_OK);
	assert_string_equal(c->cose_type, "eddsa");
	r = f2p_creden_set_cose_type(NULL, "baad");
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
}

void test_cred_set_rk(void **state){
	f2p_creden_t *c = *state;

	f2p_creden_set_rk(c, 1);
	assert_int_equal(c->rk, FIDO_OPT_TRUE);
	f2p_creden_set_rk(c, 176);
	assert_int_equal(c->rk, FIDO_OPT_TRUE);
	f2p_creden_set_rk(c, 164);
	assert_int_equal(c->rk, FIDO_OPT_TRUE);
	f2p_creden_set_rk(c, 0);
	assert_int_equal(c->rk, FIDO_OPT_FALSE);
}

void test_cred_set_uv(void **state){
	f2p_creden_t *c = *state;

	f2p_creden_set_uv(c, 1);
	assert_int_equal(c->uv, FIDO_OPT_TRUE);
	f2p_creden_set_uv(c, 176);
	assert_int_equal(c->uv, FIDO_OPT_TRUE);
	f2p_creden_set_uv(c, 164);
	assert_int_equal(c->uv, FIDO_OPT_TRUE);
	f2p_creden_set_uv(c, 0);
	assert_int_equal(c->uv, FIDO_OPT_FALSE);
}

void test_cred_set_extensions(void **state) {
	f2p_creden_t *c = *state;
	int r;

	assert_return_code(f2p_creden_set_extension(c, 0), F2P_OK);
	assert_int_equal(c->extensions, 0);
	assert_return_code(f2p_creden_set_extension(c, FIDO_EXT_HMAC_SECRET), F2P_OK);
	assert_int_equal(c->extensions, FIDO_EXT_HMAC_SECRET);
	assert_return_code(f2p_creden_set_extension(c, 0), F2P_OK);
	assert_int_equal(c->extensions, 0);
	r = f2p_creden_set_extension(NULL, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(-f2p_creden_set_extension(c, 86), -F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(-f2p_creden_set_extension(c, 74), -F2P_ERR_INVALID_ARGUMENT);
}

void test_cred_set_client_data_hash(void **state) {
	f2p_creden_t *c = *state;

	const uint8_t arr0[14] = {123, 32, 7, 8, 65, 78, 43, 4, 20, 6, 9, 0, 129, 1};
	const int arr0_len = 14;
	const uint8_t arr1[11] = {94, 29, 94, 23, 123, 90, 75, 198, 93, 65, 48};
	const int arr1_len = 11;

	assert_return_code(-f2p_creden_set_client_data_hash(c, NULL, 0), -F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(-f2p_creden_set_client_data_hash(c, arr0, 0), -F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_creden_set_client_data_hash(c, arr0, arr0_len), F2P_OK);
	assert_memory_equal(c->client_data_hash, arr0, arr0_len);
	assert_int_equal(c->client_data_hash_len, arr0_len);
	assert_return_code(f2p_creden_set_client_data_hash(c, arr1, arr1_len), F2P_OK);
	assert_memory_equal(c->client_data_hash, arr1, arr1_len);
	assert_int_equal(c->client_data_hash_len, arr1_len);
}

void test_cred_set_user_id(void **state) {
	f2p_creden_t *c = *state;
	int r;

	const uint8_t arr0[14] = {123, 32, 7, 8, 65, 78, 43, 4, 20, 6, 9, 0, 129, 1};
	const int arr0_len = 14;
	const uint8_t arr1[11] = {94, 29, 94, 23, 123, 90, 75, 198, 93, 65, 48};
	const int arr1_len = 11;

	r = f2p_creden_set_user_id(c, NULL, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_creden_set_user_id(c, arr0, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_creden_set_user_id(c, arr0, arr0_len), F2P_OK);
	assert_memory_equal(c->user_id, arr0, arr0_len);
	assert_int_equal(c->user_id_len, arr0_len);
	assert_return_code(f2p_creden_set_user_id(c, arr1, arr1_len), F2P_OK);
	assert_memory_equal(c->user_id, arr1, arr1_len);
	assert_int_equal(c->user_id_len, arr1_len);
}

void test_cred_set_authdata(void **state) {
	f2p_creden_t *c = *state;
	int r;

	const uint8_t arr0[14] = {123, 32, 7, 8, 65, 78, 43, 4, 20, 6, 9, 0, 129, 1};
	const int arr0_len = 14;
	const uint8_t arr1[11] = {94, 29, 94, 23, 123, 90, 75, 198, 93, 65, 48};
	const int arr1_len = 11;

	r = f2p_creden_set_authdata(c, NULL, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_creden_set_authdata(c, arr0, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_creden_set_authdata(c, arr0, arr0_len), F2P_OK);
	assert_memory_equal(c->authdata, arr0, arr0_len);
	assert_int_equal(c->authdata_len, arr0_len);
	assert_return_code(f2p_creden_set_authdata(c, arr1, arr1_len), F2P_OK);
	assert_memory_equal(c->authdata, arr1, arr1_len);
	assert_int_equal(c->authdata_len, arr1_len);
}

void test_cred_set_x5c(void **state) {
	f2p_creden_t *c = *state;
	int r;

	const uint8_t arr0[14] = {123, 32, 7, 8, 65, 78, 43, 4, 20, 6, 9, 0, 129, 1};
	const int arr0_len = 14;
	const uint8_t arr1[11] = {94, 29, 94, 23, 123, 90, 75, 198, 93, 65, 48};
	const int arr1_len = 11;

	r = f2p_creden_set_x5c(c, NULL, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_creden_set_x5c(c, arr0, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_creden_set_x5c(c, arr0, arr0_len), F2P_OK);
	assert_memory_equal(c->x5c, arr0, arr0_len);
	assert_int_equal(c->x5c_len, arr0_len);
	assert_return_code(f2p_creden_set_x5c(c, arr1, arr1_len), F2P_OK);
	assert_memory_equal(c->x5c, arr1, arr1_len);
	assert_int_equal(c->x5c_len, arr1_len);
}

void test_cred_set_signature(void **state) {
	f2p_creden_t *c = *state;
	int r;

	const uint8_t arr0[14] = {123, 32, 7, 8, 65, 78, 43, 4, 20, 6, 9, 0, 129, 1};
	const int arr0_len = 14;
	const uint8_t arr1[11] = {94, 29, 94, 23, 123, 90, 75, 198, 93, 65, 48};
	const int arr1_len = 11;

	r = f2p_creden_set_signature(c, NULL, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_creden_set_signature(c, arr0, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_creden_set_signature(c, arr0, arr0_len), F2P_OK);
	assert_memory_equal(c->signature, arr0, arr0_len);
	assert_int_equal(c->signature_len, arr0_len);
	assert_return_code(f2p_creden_set_signature(c, arr1, arr1_len), F2P_OK);
	assert_memory_equal(c->signature, arr1, arr1_len);
	assert_int_equal(c->signature_len, arr1_len);
}

void test_cred_set_credential_id(void **state) {
	f2p_creden_t *c = *state;
	int r;

	const uint8_t arr0[14] = {123, 32, 7, 8, 65, 78, 43, 4, 20, 6, 9, 0, 129, 1};
	const int arr0_len = 14;
	const uint8_t arr1[11] = {94, 29, 94, 23, 123, 90, 75, 198, 93, 65, 48};
	const int arr1_len = 11;

	r = f2p_creden_set_credential_id(c, NULL, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_creden_set_credential_id(c, arr0, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_creden_set_credential_id(c, arr0, arr0_len), F2P_OK);
	assert_memory_equal(c->credential_id, arr0, arr0_len);
	assert_int_equal(c->credential_id_len, arr0_len);
	assert_return_code(f2p_creden_set_credential_id(c, arr1, arr1_len), F2P_OK);
	assert_memory_equal(c->credential_id, arr1, arr1_len);
	assert_int_equal(c->credential_id_len, arr1_len);
}

void test_cred_set_public_key(void **state) {
	f2p_creden_t *c = *state;
	int r;

	const uint8_t arr0[14] = {123, 32, 7, 8, 65, 78, 43, 4, 20, 6, 9, 0, 129, 1};
	const int arr0_len = 14;
	const uint8_t arr1[11] = {94, 29, 94, 23, 123, 90, 75, 198, 93, 65, 48};
	const int arr1_len = 11;

	r = f2p_creden_set_public_key(c, NULL, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_creden_set_public_key(c, arr0, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_creden_set_public_key(c, arr0, arr0_len), F2P_OK);
	assert_memory_equal(c->public_key, arr0, arr0_len);
	assert_int_equal(c->public_key_len, arr0_len);
	assert_return_code(f2p_creden_set_public_key(c, arr1, arr1_len), F2P_OK);
	assert_memory_equal(c->public_key, arr1, arr1_len);
	assert_int_equal(c->public_key_len, arr1_len);
}

void test_cred_set_relying_party(void **state) {
	f2p_creden_t *c = *state;
	int r;

	r = f2p_creden_set_relying_party(NULL, NULL);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_creden_set_relying_party(NULL, "");
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_creden_set_relying_party(c, "data"), F2P_OK);
	assert_string_equal(c->relying_party, "data");
	assert_return_code(f2p_creden_set_relying_party(c, "other than data"), F2P_OK);
	assert_string_equal(c->relying_party, "other than data");
	r = f2p_creden_set_relying_party(NULL, NULL);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
}

void test_cred_set_fmt(void **state) {
	f2p_creden_t *c = *state;
	int r;

	r = f2p_creden_set_fmt(NULL, NULL);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_creden_set_fmt(NULL, "");
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_creden_set_fmt(c, "data"), F2P_OK);
	assert_string_equal(c->fmt, "data");
	assert_return_code(f2p_creden_set_fmt(c, "other than data"), F2P_OK);
	assert_string_equal(c->fmt, "other than data");
	r = f2p_creden_set_fmt(NULL, NULL);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
}

void test_cred_set_user_display_name(void **state) {
	f2p_creden_t *c = *state;
	int r;

	r = f2p_creden_set_user_display_name(NULL, NULL);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_creden_set_user_display_name(NULL, "");
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_creden_set_user_display_name(c, "data"), F2P_OK);
	assert_string_equal(c->user_display_name, "data");
	assert_return_code(f2p_creden_set_user_display_name(c, "other than data"), F2P_OK);
	assert_string_equal(c->user_display_name, "other than data");
	r = f2p_creden_set_user_display_name(NULL, NULL);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
}

void test_cred_set_user_icon(void **state) {
	f2p_creden_t *c = *state;
	int r;

	r = f2p_creden_set_user_icon(NULL, NULL);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_creden_set_user_icon(NULL, "");
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_creden_set_user_icon(c, "data"), F2P_OK);
	assert_string_equal(c->user_icon, "data");
	assert_return_code(f2p_creden_set_user_icon(c, "other than data"), F2P_OK);
	assert_string_equal(c->user_icon, "other than data");
	r = f2p_creden_set_user_icon(NULL, NULL);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
}

void test_cred_set_user_name(void **state) {
	f2p_creden_t *c = *state;
	int r;

	r = f2p_creden_set_user_name(NULL, NULL);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_creden_set_user_name(NULL, "");
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_creden_set_user_name(c, "data"), F2P_OK);
	assert_string_equal(c->user_name, "data");
	assert_return_code(f2p_creden_set_user_name(c, "other than data"), F2P_OK);
	assert_string_equal(c->user_name, "other than data");
	r = f2p_creden_set_user_name(NULL, NULL);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
}

void test_cred_set_required(void **state) {
	f2p_creden_t *c = *state;
	int r;

	const uint8_t arr0[14] = {123, 32, 7, 8, 65, 78, 43, 4, 20, 6, 9, 0, 129, 1};
	const int arr0_len = 14;
	const uint8_t arr1[11] = {94, 29, 94, 23, 123, 90, 75, 198, 93, 65, 48};
	const int arr1_len = 11;

	const char *s0 = "some string";
	const char *s1 = "another test string";

	r = f2p_creden_set_required(NULL, NULL, NULL, 0, NULL, NULL, 0, NULL);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_creden_set_required(c, NULL, NULL, 0, NULL, NULL, 0, NULL);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_creden_set_required(c, s0, arr0, arr0_len, s0, arr0, arr0_len, s0), F2P_OK);
	assert_string_equal(c->cose_type, s0);
	assert_string_equal(c->relying_party, s0);
	assert_string_equal(c->user_name, s0);
	assert_memory_equal(c->client_data_hash, arr0, arr0_len);
	assert_memory_equal(c->user_id, arr0, arr0_len);

	assert_return_code(f2p_creden_set_required(c, s1, arr1, arr1_len, s1, arr1, arr1_len, s1), F2P_OK);
	assert_string_equal(c->cose_type, s1);
	assert_string_equal(c->relying_party, s1);
	assert_string_equal(c->user_name, s1);
	assert_memory_equal(c->client_data_hash, arr1, arr1_len);
	assert_memory_equal(c->user_id, arr1, arr1_len);

	r = f2p_creden_set_required(c, NULL, NULL, 0, NULL, NULL, 0, NULL);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_creden_set_required(c, s1, arr1, arr1_len, s1, arr1, arr1_len, s1), F2P_OK);
	assert_string_equal(c->cose_type, s1);
	assert_string_equal(c->relying_party, s1);
	assert_string_equal(c->user_name, s1);
	assert_memory_equal(c->client_data_hash, arr1, arr1_len);
	assert_memory_equal(c->user_id, arr1, arr1_len);
}

/* ---------------------------------------------------------------------------------------------------------- */

void test_assert_set_up(void **state){
	f2p_assert_t *a = *state;

	f2p_assert_set_up(a, 1);
	assert_int_equal(a->up, FIDO_OPT_TRUE);
	f2p_assert_set_up(a, 176);
	assert_int_equal(a->up, FIDO_OPT_TRUE);
	f2p_assert_set_up(a, 164);
	assert_int_equal(a->up, FIDO_OPT_TRUE);
	f2p_assert_set_up(a, 0);
	assert_int_equal(a->up, FIDO_OPT_FALSE);
}

void test_assert_set_uv(void **state){
	f2p_assert_t *a = *state;

	f2p_assert_set_uv(a, 1);
	assert_int_equal(a->uv, FIDO_OPT_TRUE);
	f2p_assert_set_uv(a, 176);
	assert_int_equal(a->uv, FIDO_OPT_TRUE);
	f2p_assert_set_uv(a, 164);
	assert_int_equal(a->uv, FIDO_OPT_TRUE);
	f2p_assert_set_uv(a, 0);
	assert_int_equal(a->uv, FIDO_OPT_FALSE);
}

void test_assert_set_extensions(void **state) {
	f2p_assert_t *a = *state;
	int r;

	assert_return_code(f2p_assert_set_extension(a, 0), F2P_OK);
	assert_int_equal(a->extensions, 0);
	assert_return_code(f2p_assert_set_extension(a, FIDO_EXT_HMAC_SECRET), F2P_OK);
	assert_int_equal(a->extensions, FIDO_EXT_HMAC_SECRET);
	assert_return_code(f2p_assert_set_extension(a, 0), F2P_OK);
	assert_int_equal(a->extensions, 0);
	r = f2p_assert_set_extension(NULL, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_assert_set_extension(a, 86);
       	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_assert_set_extension(a, 74);
       	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
}

void test_assert_set_count(void **state) {
	f2p_assert_t *a = *state;

	f2p_assert_set_count(a, 0);
	assert_int_equal(a->count, 0);
	f2p_assert_set_count(a, 42);
	assert_int_equal(a->count, 42);
	f2p_assert_set_count(a, 69);
	assert_int_equal(a->count, 69);
}

void test_assert_set_sig_count(void **state) {
	f2p_assert_t *a = *state;

	f2p_assert_set_sig_count(a, 0);
	assert_int_equal(a->sig_count, 0);
	f2p_assert_set_sig_count(a, 42);
	assert_int_equal(a->sig_count, 42);
	f2p_assert_set_sig_count(a, 69);
	assert_int_equal(a->sig_count, 69);
}

void test_assert_set_client_data_hash(void **state) {
	f2p_assert_t *a = *state;
	int r;

	const uint8_t arr0[14] = {123, 32, 7, 8, 65, 78, 43, 4, 20, 6, 9, 0, 129, 1};
	const int arr0_len = 14;
	const uint8_t arr1[11] = {94, 29, 94, 23, 123, 90, 75, 198, 93, 65, 48};
	const int arr1_len = 11;

	r = f2p_assert_set_client_data_hash(a, NULL, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_assert_set_client_data_hash(a, arr0, arr0_len), F2P_OK);
	assert_memory_equal(a->client_data_hash, arr0, arr0_len);
	assert_int_equal(a->client_data_hash_len, arr0_len);
	assert_return_code(f2p_assert_set_client_data_hash(a, arr1, arr1_len), F2P_OK);
	assert_memory_equal(a->client_data_hash, arr1, arr1_len);
	assert_int_equal(a->client_data_hash_len, arr1_len);
}

void test_assert_set_allow_credential(void **state) {
	f2p_assert_t *a = *state;
	int r;

	const uint8_t arr0[14] = {123, 32, 7, 8, 65, 78, 43, 4, 20, 6, 9, 0, 129, 1};
	const int arr0_len = 14;
	const uint8_t arr1[11] = {94, 29, 94, 23, 123, 90, 75, 198, 93, 65, 48};
	const int arr1_len = 11;

	r = f2p_assert_set_allow_credential(a, NULL, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_assert_set_allow_credential(a, arr0, arr0_len), F2P_OK);
	assert_memory_equal(a->allow_credential, arr0, arr0_len);
	assert_int_equal(a->allow_credential_len, arr0_len);
	assert_return_code(f2p_assert_set_allow_credential(a, arr1, arr1_len), F2P_OK);
	assert_memory_equal(a->allow_credential, arr1, arr1_len);
	assert_int_equal(a->allow_credential_len, arr1_len);
}

void test_assert_set_authdata(void **state) {
	f2p_assert_t *a = *state;
	int r;

	const uint8_t arr0[14] = {123, 32, 7, 8, 65, 78, 43, 4, 20, 6, 9, 0, 129, 1};
	const int arr0_len = 14;
	const uint8_t arr1[11] = {94, 29, 94, 23, 123, 90, 75, 198, 93, 65, 48};
	const int arr1_len = 11;

	r = f2p_assert_set_authdata(a, NULL, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_assert_set_authdata(a, arr0, arr0_len), F2P_OK);
	assert_memory_equal(a->authdata, arr0, arr0_len);
	assert_int_equal(a->authdata_len, arr0_len);
	assert_return_code(f2p_assert_set_authdata(a, arr1, arr1_len), F2P_OK);
	assert_memory_equal(a->authdata, arr1, arr1_len);
	assert_int_equal(a->authdata_len, arr1_len);
}

void test_assert_set_hmac_secret(void **state) {
	f2p_assert_t *a = *state;
	int r;

	const uint8_t arr0[14] = {123, 32, 7, 8, 65, 78, 43, 4, 20, 6, 9, 0, 129, 1};
	const int arr0_len = 14;
	const uint8_t arr1[11] = {94, 29, 94, 23, 123, 90, 75, 198, 93, 65, 48};
	const int arr1_len = 11;

	r = f2p_assert_set_hmac_secret(a, NULL, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_assert_set_hmac_secret(a, arr0, arr0_len), F2P_OK);
	assert_memory_equal(a->hmac_secret, arr0, arr0_len);
	assert_int_equal(a->hmac_secret_len, arr0_len);
	assert_return_code(f2p_assert_set_hmac_secret(a, arr1, arr1_len), F2P_OK);
	assert_memory_equal(a->hmac_secret, arr1, arr1_len);
	assert_int_equal(a->hmac_secret_len, arr1_len);
}

void test_assert_set_signature(void **state) {
	f2p_assert_t *a = *state;
	int r;

	const uint8_t arr0[14] = {123, 32, 7, 8, 65, 78, 43, 4, 20, 6, 9, 0, 129, 1};
	const int arr0_len = 14;
	const uint8_t arr1[11] = {94, 29, 94, 23, 123, 90, 75, 198, 93, 65, 48};
	const int arr1_len = 11;

	r = f2p_assert_set_signature(a, NULL, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_assert_set_signature(a, arr0, arr0_len), F2P_OK);
	assert_memory_equal(a->signature, arr0, arr0_len);
	assert_int_equal(a->signature_len, arr0_len);
	assert_return_code(f2p_assert_set_signature(a, arr1, arr1_len), F2P_OK);
	assert_memory_equal(a->signature, arr1, arr1_len);
	assert_int_equal(a->signature_len, arr1_len);
}

void test_assert_set_user_id(void **state) {
	f2p_assert_t *a = *state;
	int r;

	const uint8_t arr0[14] = {123, 32, 7, 8, 65, 78, 43, 4, 20, 6, 9, 0, 129, 1};
	const int arr0_len = 14;
	const uint8_t arr1[11] = {94, 29, 94, 23, 123, 90, 75, 198, 93, 65, 48};
	const int arr1_len = 11;

	r = f2p_assert_set_user_id(a, NULL, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_assert_set_user_id(a, arr0, arr0_len), F2P_OK);
	assert_memory_equal(a->user_id, arr0, arr0_len);
	assert_int_equal(a->user_id_len, arr0_len);
	assert_return_code(f2p_assert_set_user_id(a, arr1, arr1_len), F2P_OK);
	assert_memory_equal(a->user_id, arr1, arr1_len);
	assert_int_equal(a->user_id_len, arr1_len);
}

void test_assert_set_relying_party(void **state) {
	f2p_assert_t *a = *state;
	int r;

	r = f2p_assert_set_relying_party(NULL, NULL);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_assert_set_relying_party(NULL, "");
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_assert_set_relying_party(a, "data"), F2P_OK);
	assert_string_equal(a->relying_party, "data");
	assert_return_code(f2p_assert_set_relying_party(a, "other than data"), F2P_OK);
	assert_string_equal(a->relying_party, "other than data");
}

void test_assert_set_user_display_name(void **state) {
	f2p_assert_t *a = *state;
	int r;

	r = f2p_assert_set_user_display_name(NULL, NULL);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_assert_set_user_display_name(NULL, "");
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_assert_set_user_display_name(a, "data"), F2P_OK);
	assert_string_equal(a->user_display_name, "data");
	assert_return_code(f2p_assert_set_user_display_name(a, "other than data"), F2P_OK);
	assert_string_equal(a->user_display_name, "other than data");
}

void test_assert_set_user_icon(void **state) {
	f2p_assert_t *a = *state;
	int r;

	r = f2p_assert_set_user_icon(NULL, NULL);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_assert_set_user_icon(NULL, "");
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_assert_set_user_icon(a, "data"), F2P_OK);
	assert_string_equal(a->user_icon, "data");
	assert_return_code(f2p_assert_set_user_icon(a, "other than data"), F2P_OK);
	assert_string_equal(a->user_icon, "other than data");
}

void test_assert_set_user_name(void **state) {
	f2p_assert_t *a = *state;
	int r;

	r = f2p_assert_set_user_name(NULL, NULL);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_assert_set_user_name(NULL, "");
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_assert_set_user_name(a, "data"), F2P_OK);
	assert_string_equal(a->user_name, "data");
	assert_return_code(f2p_assert_set_user_name(a, "other than data"), F2P_OK);
	assert_string_equal(a->user_name, "other than data");
}

void test_assert_set_required(void **state) {
	f2p_assert_t *a = *state;
	int r;

	const uint8_t arr0[14] = {123, 32, 7, 8, 65, 78, 43, 4, 20, 6, 9, 0, 129, 1};
	const int arr0_len = 14;
	const uint8_t arr1[11] = {94, 29, 94, 23, 123, 90, 75, 198, 93, 65, 48};
	const int arr1_len = 11;

	const char *s0 = "some string";
	const char *s1 = "another test string";

	r = f2p_assert_set_required(NULL, NULL, 0, NULL, NULL, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	r = f2p_assert_set_required(a, NULL, 0, NULL, NULL, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_assert_set_required(a, arr0, arr0_len, s0, arr0, arr0_len), F2P_OK);
	assert_string_equal(a->relying_party, s0);
	assert_memory_equal(a->client_data_hash, arr0, arr0_len);
	assert_memory_equal(a->allow_credential, arr0, arr0_len);

	assert_return_code(f2p_assert_set_required(a, arr1, arr1_len, s1, arr1, arr1_len), F2P_OK);
	assert_string_equal(a->relying_party, s1);
	assert_memory_equal(a->client_data_hash, arr1, arr1_len);
	assert_memory_equal(a->allow_credential, arr1, arr1_len);

	r = f2p_assert_set_required(a, NULL, 0, NULL, NULL, 0);
	assert_int_equal(r, F2P_ERR_INVALID_ARGUMENT);
	assert_return_code(f2p_assert_set_required(a, arr1, arr1_len, s1, arr1, arr1_len), F2P_OK);
	assert_string_equal(a->relying_party, s1);
	assert_memory_equal(a->client_data_hash, arr1, arr1_len);
	assert_memory_equal(a->allow_credential, arr1, arr1_len);
}

/* ---------------------------------------------------------------------------------------------------------- */

static int
test_pin_cb(int pipe_write) {
	return mock();
}

static int
error_callback(sd_bus_message *m, void* data, sd_bus_error *err) {
	return 0;
}

void
test_parse_cred_response(void **state) {
	sd_bus_message *reply;
	f2p_creden_t *c = f2p_creden_init();
	sd_bus *bus;
	int r;

	sd_bus_default_user(&bus);

	sd_bus_message_new_signal(bus, &reply, F2P_SERVER_DBUS_REQUEST_OBJECT, \
			F2P_SERVER_DBUS_REQUEST_INTERFACE, "bad_member");
	r = parse_cred_response(reply, c, test_pin_cb, error_callback);
	assert_int_equal(r, F2P_ERR_NO_MEMBER);
	sd_bus_message_unrefp(&reply);

	sd_bus_message_new_signal(bus, &reply, F2P_SERVER_DBUS_REQUEST_OBJECT, \
			F2P_SERVER_DBUS_REQUEST_INTERFACE, F2P_SERVER_MEMBER_PIN);
	r = parse_cred_response(reply, c, test_pin_cb, error_callback);
	assert_int_equal(r, F2P_ERR_BUS_MSG);
	sd_bus_message_unrefp(&reply);

	sd_bus_message_new_signal(bus, &reply, F2P_SERVER_DBUS_REQUEST_OBJECT, \
			F2P_SERVER_DBUS_REQUEST_INTERFACE, F2P_SERVER_MEMBER_PIN);
	sd_bus_message_append_basic(reply, 's', "not a pipe and wrong type");
	r = parse_cred_response(reply, c, test_pin_cb, error_callback);
	assert_int_equal(r, F2P_ERR_BUS_MSG);
	sd_bus_message_unrefp(&reply);

	sd_bus_message_new_signal(bus, &reply, F2P_SERVER_DBUS_REQUEST_OBJECT, \
			F2P_SERVER_DBUS_REQUEST_INTERFACE, F2P_SERVER_MEMBER_PIN);
	int i[2];
	pipe2(i, 0);
	sd_bus_message_append(reply, "h", i[0]);
	sd_bus_send(bus, reply, NULL);
	will_return(test_pin_cb, -1);
	r = parse_cred_response(reply, c, test_pin_cb, error_callback);
	assert_int_equal(r, F2P_ERR_PIN_NOT_SET);
	sd_bus_message_unrefp(&reply);

	sd_bus_message_new_signal(bus, &reply, F2P_SERVER_DBUS_REQUEST_OBJECT, \
			F2P_SERVER_DBUS_REQUEST_INTERFACE, F2P_SERVER_MEMBER_PIN);
	sd_bus_message_append(reply, "h", i[0]);
	sd_bus_send(bus, reply, NULL);
	will_return(test_pin_cb, 0);
	r = parse_cred_response(reply, c, test_pin_cb, error_callback);
	assert_int_equal(r, 0);
	sd_bus_message_unrefp(&reply);
	close(i[0]);
	close(i[1]);

	sd_bus_message_new_signal(bus, &reply, F2P_SERVER_DBUS_REQUEST_OBJECT, \
			F2P_SERVER_DBUS_REQUEST_INTERFACE, F2P_SERVER_MEMBER_ERR);
	r = parse_cred_response(reply, c, test_pin_cb, NULL);
	assert_int_equal(r, F2P_ERR_SERVER);
	sd_bus_message_unrefp(&reply);

	sd_bus_message_new_signal(bus, &reply, F2P_SERVER_DBUS_REQUEST_OBJECT, \
			F2P_SERVER_DBUS_REQUEST_INTERFACE, F2P_SERVER_MEMBER_ERR);
	r = parse_cred_response(reply, c, test_pin_cb, error_callback);
	assert_int_equal(r, F2P_ERR_SERVER);
	sd_bus_message_unrefp(&reply);

	sd_bus_flush_close_unrefp(&bus);
	f2p_creden_free(&c);
}

void
test_parse_assert_response(void **state) {
	sd_bus_message *reply;
	f2p_assert_t *a = f2p_assert_init();
	sd_bus *bus;
	int r;

	sd_bus_default_user(&bus);

	sd_bus_message_new_signal(bus, &reply, F2P_SERVER_DBUS_REQUEST_OBJECT, \
			F2P_SERVER_DBUS_REQUEST_INTERFACE, "bad_member");
	r = parse_assert_response(reply, a, error_callback);
	assert_int_equal(r, F2P_ERR_NO_MEMBER);
	sd_bus_message_unrefp(&reply);

	sd_bus_message_new_signal(bus, &reply, F2P_SERVER_DBUS_REQUEST_OBJECT, \
			F2P_SERVER_DBUS_REQUEST_INTERFACE, F2P_SERVER_MEMBER_ERR); 
	r = parse_assert_response(reply, a, error_callback);
	assert_int_equal(r, F2P_ERR_SERVER);
	sd_bus_message_unrefp(&reply);

	sd_bus_message_new_signal(bus, &reply, F2P_SERVER_DBUS_REQUEST_OBJECT, \
			F2P_SERVER_DBUS_REQUEST_INTERFACE, F2P_SERVER_MEMBER_ERR); 
	r = parse_assert_response(reply, a, NULL);
	assert_int_equal(r, F2P_ERR_SERVER);
	sd_bus_message_unrefp(&reply);
}

void
test_set_uint8_array(void **state) {
	int r;
	uint8_t *arr = NULL;
	const uint8_t s1[6] = {1, 2, 3, 4, 5, 6};
	const uint8_t s2[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	const uint8_t s3[3] = {1, 5, 6};
	const uint8_t s4[11] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

	r = set_uint8_array(&arr, s1, 6);
	assert_int_equal(r, F2P_OK);
	r = set_uint8_array(&arr, s2, 10);
	assert_int_equal(r, F2P_OK);
	r = set_uint8_array(&arr, s3, 3);
	assert_int_equal(r, F2P_OK);
	r = set_uint8_array(&arr, s4, 12);
	assert_int_equal(r, F2P_OK);
	r = set_uint8_array(&arr, s1, 3);
	assert_int_equal(r, F2P_OK);

	if(arr)
		free(arr);
}

void
test_set_string(void **state) {
	int r;
	char *s = NULL;
	const char *s1 = "This is some string";
	const char *s2 = "Testing";
	const char *s3 = "I am your butterfly I need your protection Be my samurai";

	r = set_string(&s, s1);
	assert_int_equal(r, F2P_OK);
	r = set_string(&s, s2);
	assert_int_equal(r, F2P_OK);
	r = set_string(&s, s3);
	assert_int_equal(r, F2P_OK);
	r = set_string(&s, s1);
	assert_int_equal(r, F2P_OK);

	free(s);
}

/* ---------------------------------------------------------------------------------------------------------- */

static int
setup_cred(void **state) {
	f2p_creden_t *c = f2p_creden_init();
	if (!c)
		return -1;

	*state = c;

	return 0;
}

static int
teardown_cred(void **state) {
	f2p_creden_free((f2p_creden_t **)state);

	return 0;
}

static int
setup_assert(void **state) {
	f2p_assert_t *a = f2p_assert_init();
	if (!a)
		return -1;

	*state = a;

	return 0;
}

static int
teardown_assert(void **state) {
	f2p_assert_free((f2p_assert_t **)state);

	return 0;
}

int
main (void) {
	const struct CMUnitTest tests[] = {
		cmocka_unit_test_setup_teardown(test_cred_set_cose_type, setup_cred, teardown_cred),
		cmocka_unit_test_setup_teardown(test_cred_set_rk, setup_cred, teardown_cred),
		cmocka_unit_test_setup_teardown(test_cred_set_uv, setup_cred, teardown_cred),
		cmocka_unit_test_setup_teardown(test_cred_set_extensions, setup_cred, teardown_cred),
		cmocka_unit_test_setup_teardown(test_cred_set_client_data_hash, setup_cred, teardown_cred),
		cmocka_unit_test_setup_teardown(test_cred_set_user_id, setup_cred, teardown_cred),
		cmocka_unit_test_setup_teardown(test_cred_set_authdata, setup_cred, teardown_cred),
		cmocka_unit_test_setup_teardown(test_cred_set_x5c, setup_cred, teardown_cred),
		cmocka_unit_test_setup_teardown(test_cred_set_signature, setup_cred, teardown_cred),
		cmocka_unit_test_setup_teardown(test_cred_set_credential_id, setup_cred, teardown_cred),
		cmocka_unit_test_setup_teardown(test_cred_set_public_key, setup_cred, teardown_cred),
		cmocka_unit_test_setup_teardown(test_cred_set_relying_party, setup_cred, teardown_cred),
		cmocka_unit_test_setup_teardown(test_cred_set_fmt, setup_cred, teardown_cred),
		cmocka_unit_test_setup_teardown(test_cred_set_user_display_name, setup_cred, teardown_cred),
		cmocka_unit_test_setup_teardown(test_cred_set_user_icon, setup_cred, teardown_cred),
		cmocka_unit_test_setup_teardown(test_cred_set_user_name, setup_cred, teardown_cred),
		cmocka_unit_test_setup_teardown(test_cred_set_required, setup_cred, teardown_cred),
		//
		cmocka_unit_test_setup_teardown(test_assert_set_up, setup_assert, teardown_assert),
		cmocka_unit_test_setup_teardown(test_assert_set_uv, setup_assert, teardown_assert),
		cmocka_unit_test_setup_teardown(test_assert_set_extensions, setup_assert, teardown_assert),
		cmocka_unit_test_setup_teardown(test_assert_set_count, setup_assert, teardown_assert),
		cmocka_unit_test_setup_teardown(test_assert_set_sig_count, setup_assert, teardown_assert),
		cmocka_unit_test_setup_teardown(test_assert_set_client_data_hash, setup_assert, teardown_assert),
		cmocka_unit_test_setup_teardown(test_assert_set_allow_credential, setup_assert, teardown_assert),
		cmocka_unit_test_setup_teardown(test_assert_set_authdata, setup_assert, teardown_assert),
		cmocka_unit_test_setup_teardown(test_assert_set_hmac_secret, setup_assert, teardown_assert),
		cmocka_unit_test_setup_teardown(test_assert_set_signature, setup_assert, teardown_assert),
		cmocka_unit_test_setup_teardown(test_assert_set_user_id, setup_assert, teardown_assert),
		cmocka_unit_test_setup_teardown(test_assert_set_relying_party, setup_assert, teardown_assert),
		cmocka_unit_test_setup_teardown(test_assert_set_user_display_name, setup_assert, teardown_assert),
		cmocka_unit_test_setup_teardown(test_assert_set_user_icon, setup_assert, teardown_assert),
		cmocka_unit_test_setup_teardown(test_assert_set_user_name, setup_assert, teardown_assert),
		cmocka_unit_test_setup_teardown(test_assert_set_required, setup_assert, teardown_assert),
		cmocka_unit_test(test_parse_cred_response),
		cmocka_unit_test(test_parse_assert_response),
		cmocka_unit_test(test_set_uint8_array),
		cmocka_unit_test(test_set_string)
	};

	return cmocka_run_group_tests(tests, NULL, NULL);
}
